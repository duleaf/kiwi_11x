//
//  StoreLocationViewController.h
//  Kiwi
//
//  Created by Mohammed Salah on ٧‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationView.h"
#import "ListOfStores.h"
@interface StoreLocationViewController : UIViewController <UISearchDisplayDelegate>
{
    LocationView * locationView;
    ListOfStores * listOfStores;
    NSMutableArray * stors;
}

@property(nonatomic,strong) IBOutlet UIView *searchContainer;
@property(nonatomic,strong) IBOutlet UISegmentedControl *segment;
@property(nonatomic,strong) IBOutlet UISearchDisplayController *searchController;
@property(nonatomic,strong) IBOutlet UIView *viewContainer;


@end
