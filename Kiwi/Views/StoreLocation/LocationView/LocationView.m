//
//  LocationView.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "LocationView.h"
#import "VBAnnotation.h"


#define RE_LATITUDE 36.7478
#define RE_LONGITUDE -119.7714

@implementation LocationView


@synthesize iMapView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)awakeFromNib
{

}


-(void)drawLocations
{
    MKCoordinateRegion region;
    region.center.latitude = RE_LATITUDE;
    region.center.longitude = RE_LONGITUDE;
    region.span.latitudeDelta = 2;
    region.span.longitudeDelta = 2;
    [iMapView setRegion:region animated:NO];
    // create annotations
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    CLLocationCoordinate2D location;
    VBAnnotation *annotation;
    
    location = [self getLocation];
    
    
    

    
    for(id object in _locations)
    {
        
        location.latitude = [(NSString*)[object valueForKey:@"lat"] doubleValue];
        location.longitude = [(NSString*)[object valueForKey:@"lang"] doubleValue];
        
        annotation = [[VBAnnotation alloc] init];
        [annotation setCoordinate:location];
        annotation.title = (NSString*)[object valueForKey:@"name"];
        [annotations addObject:annotation];
    }
    
    [iMapView addAnnotations:annotations];
    
    [iMapView showAnnotations:annotations animated:YES];
    
    iMapView.camera.altitude *= 1.4;
}

-(CLLocationCoordinate2D) getLocation{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init ];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    // [locationManager startUpdatingLocation];
    
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    // [locationManager startUpdatingLocation];
    return coordinate;
}
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    NSLog(@"lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *identifier = @"pin";
    MKAnnotationView *view = [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (view == nil) {
        view = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
    }
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sun.png"]];
    view.leftCalloutAccessoryView = imageView;
    
    view.enabled = YES;
    view.canShowCallout = YES;
    return view;
}

@end
