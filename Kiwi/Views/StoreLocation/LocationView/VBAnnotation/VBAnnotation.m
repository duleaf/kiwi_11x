//
//  VBAnnotation.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "VBAnnotation.h"

@implementation VBAnnotation

@synthesize coordinate;
@synthesize title;
@synthesize subtitle;

- initWithPosition:(CLLocationCoordinate2D)coords {
    if (self = [super init]) {
        self.coordinate = coords;
    }
    return self;
}

@end
