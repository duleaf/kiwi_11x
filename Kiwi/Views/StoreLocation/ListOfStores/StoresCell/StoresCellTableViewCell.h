//
//  StoresCellTableViewCell.h
//  Kiwi
//
//  Created by Mohammed Salah on ٧‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoresCellTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel * title;
@property (nonatomic,strong) IBOutlet UILabel * address;
@property (nonatomic,strong) IBOutlet UILabel * distance;
@property (nonatomic,strong) NSMutableArray   * services;


@end
