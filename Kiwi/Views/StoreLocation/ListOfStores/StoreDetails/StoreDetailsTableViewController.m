
//
//  StoreDetailsTableViewController.m
//  Kiwi
//
//  Created by Mohammed Salah on ٧‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "StoreDetailsTableViewController.h"
#import "StoresCellTableViewCell.h"
@interface StoreDetailsTableViewController ()
{
    NSMutableArray *reuseIdentifier;
}
@end

@implementation StoreDetailsTableViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _storeTitel.text = _store.name;
//    _address.text = _store.storeAddress;
//    _phone.text = _store.storePhone;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
