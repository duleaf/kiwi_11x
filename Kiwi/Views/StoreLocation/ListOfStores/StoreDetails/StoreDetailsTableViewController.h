//
//  StoreDetailsTableViewController.h
//  Kiwi
//
//  Created by Mohammed Salah on ٧‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KiwiStore.h"
@interface StoreDetailsTableViewController : UIViewController

@property (nonatomic,strong) KiwiStore * store;

@property (nonatomic,strong) IBOutlet UILabel * storeTitel;
@property (nonatomic,strong) IBOutlet UILabel * subTitle;
@property (nonatomic,strong) IBOutlet UILabel * address;
@property (nonatomic,strong) IBOutlet UILabel * phone;
@property (nonatomic,strong) IBOutlet UILabel * hours;


@end
