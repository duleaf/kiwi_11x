//
//  StoreDetailsTableViewCell.h
//  Kiwi
//
//  Created by Mohammed Salah on ٧‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreDetailsTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIButton *setAsMyStore;
@property (nonatomic,strong) IBOutlet UIButton *addToContacts;
@property (nonatomic,strong) IBOutlet UIButton *emailInfo;
@property (nonatomic,strong) IBOutlet UIButton *inStoreMap;
@property (nonatomic,strong) IBOutlet UIButton *sendFeedback;
@property (nonatomic,strong) IBOutlet UILabel  *title;


@end
