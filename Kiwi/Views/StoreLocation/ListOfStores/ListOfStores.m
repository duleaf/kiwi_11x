//
//  ListOfStores.m
//  Kiwi
//
//  Created by Mohammed Salah on ٧‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "ListOfStores.h"
#import "StoresCellTableViewCell.h"
#import "StoreDetailsTableViewController.h"
#import "KiwiStore.h"
@implementation ListOfStores

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}



-(void) setStores:(NSMutableArray *)stores
{
    _stores = stores;
    
    [_table reloadData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_stores count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%i",(int)indexPath.row];
    StoresCellTableViewCell *cell = [_table dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {

            cell = [[[NSBundle mainBundle] loadNibNamed:@"ListOfStores" owner:nil options:nil] objectAtIndex:1];
            [cell setRestorationIdentifier: CellIdentifier];
    }
    
    KiwiStore * store = [_stores objectAtIndex:indexPath.row];
    
    cell.title.text = store.name;
    cell.address.text = store.storeAddress;
    cell.distance.text = @"2.4 miles";
  
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    StoreDetailsTableViewController * storeDtails = [_controller.storyboard instantiateViewControllerWithIdentifier:@"storedetials"] ;
    
    storeDtails.store = [_stores objectAtIndex:indexPath.row];
    
    [_controller.navigationController pushViewController:storeDtails animated:YES];
   
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 102;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
