//
//  ListOfStores.h
//  Kiwi
//
//  Created by Mohammed Salah on ٧‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListOfStores : UIView

@property (nonatomic,strong) IBOutlet UITableView * table;
@property (nonatomic,strong) NSMutableArray * stores;
@property (nonatomic,strong) UIViewController * controller;

@end
