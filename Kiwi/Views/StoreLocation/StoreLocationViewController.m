//
//  StoreLocationViewController.m
//  Kiwi
//
//  Created by Mohammed Salah on ٧‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "StoreLocationViewController.h"
#import "KiwiStore.h"
@interface StoreLocationViewController ()

@end

@implementation StoreLocationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadStors];
    
    
    self.searchDisplayController.searchBar.delegate = self;
    [self.searchDisplayController setSearchResultsDataSource:self];
    [self.searchDisplayController setSearchResultsDelegate:self];
    
//    self.searchBar.searchBar.backgroundImage = [Utitlities imageWithColor:DEFUALT_COLOR];
    
  
}

- (void) searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    self.searchContainer.frame = CGRectMake(0, 0, 320, 64);
    self.segment.hidden = YES;

    [self.navigationController.navigationBar setHidden:YES];
}


- (void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    [self.navigationController.navigationBar setHidden:NO];
    self.searchContainer.frame = CGRectMake(0, 64, 320, 64);
    self.segment.hidden = NO;
    self.searchDisplayController.searchBar.frame = CGRectMake(0, 0, 230, 44);

}

-(IBAction)segemtnSelected:(id)sender
{
    [UIView beginAnimations:nil context:NULL];
    
    [UIView setAnimationDuration:.8];
    

    
    if(_segment.selectedSegmentIndex == 1)
    {
        if([locationView superview])
        {
            [UIView setAnimationTransition:([listOfStores superview] ? UIViewAnimationTransitionFlipFromLeft : UIViewAnimationTransitionFlipFromRight)
                                   forView:self.viewContainer
                                     cache:YES];
            [locationView removeFromSuperview];
            [self.viewContainer addSubview:listOfStores];
            
        }
    }
   
    else {
        if([listOfStores superview])
        {
            [UIView setAnimationTransition:([locationView superview] ? UIViewAnimationTransitionFlipFromLeft : UIViewAnimationTransitionFlipFromRight )
                                   forView:self.viewContainer
                                     cache:YES];
            [listOfStores removeFromSuperview];
            [self.viewContainer addSubview:locationView];
            
        }
        
    }
    
    
    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView commitAnimations];
    
}

-(void)loadStors
{
    stors = [[NSMutableArray alloc] init];
    
    KiwiStore *store = [[KiwiStore alloc] init];
    
    store.name = @"College Point";
    store.storeAddress = @"13505 20th Ave";
    store.lat = @"25.1138498";
    store.lang = @"55.1629823";
    
    [stors addObject:store];
    
    
    store = [[KiwiStore alloc] init];
    
    store.name = @"Flushing";
    store.storeAddress = @"4024 College Point Blvd";
    store.lat = @"25.1013367";
    store.lang = @"55.1603215";
    
    [stors addObject:store];
    
    
    
    locationView = [[[NSBundle mainBundle] loadNibNamed:@"LocationView" owner:nil options:nil] firstObject];
    listOfStores = [[[NSBundle mainBundle] loadNibNamed:@"ListOfStores" owner:nil options:nil] firstObject];
    
    listOfStores.controller = self;
    
    locationView.locations = [NSMutableArray arrayWithArray:stors];
    [listOfStores setStores:[NSMutableArray arrayWithArray:stors]];
    
    int viewHeight = self.view.frame.size.height - self.searchContainer.frame.origin.y;
    
    locationView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.viewContainer.frame.size.height);
    
    [self.viewContainer addSubview:listOfStores];
    [locationView drawLocations];
    
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    
    cell.textLabel.text = @"Your Location";
    
    return cell;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
