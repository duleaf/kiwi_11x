//
//  TopDetails.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٠‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "TopDetails.h"
#import "Utitlities.h"
#import "CartController.h"

@implementation TopDetails

ProductSize * sizes;

static int startPoint;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


+(int) getH
{
    return startPoint;
}



-(void)setProductDetails:(KiwiProductDetails *)product
{
    self.product = product;
    startPoint = 0;

    [self setupScrollerImager];
    [self setupTitles];
    
    
    self.colors = [[NSMutableArray alloc] init];

    for(KiwiAttribute * attribute in self.product.attribute)
    {
        if([attribute.type isEqualToString:@"Color"])
            [self.colors addObject:attribute];
    }
   
        self.imageScroll = [[[NSBundle mainBundle] loadNibNamed:@"ImagesScroll" owner:nil options:nil] firstObject];
        self.imageScroll.delegate = self;
        self.imageScroll.images = self.colors;
        self.imageScroll.frame = CGRectMake(0, startPoint + 10, self.imageScroll.scrollView.frame.size.width, self.imageScroll.scrollView.frame.size.height);
    
        [self.imageScroll buildScrollWithType:ColorImageViewer andImages:self.colors WithFrame:self.imageScroll.frame];
    
        UILabel * colorLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, startPoint, 150, 20)];
        colorLabel.text = @"Color:";
        colorLabel.font = [UIFont boldSystemFontOfSize:15];
        [colorLabel sizeToFit];
        
        UILabel * selectedColorLabel = [[UILabel alloc] initWithFrame:CGRectMake(colorLabel.frame.size.width + 20, startPoint , 150, 20)];
        selectedColorLabel.text = @"select color";
        selectedColorLabel.font = [UIFont systemFontOfSize:15];
        selectedColorLabel.textColor = [UIColor lightGrayColor];
        [selectedColorLabel sizeToFit];
        
        [self addSubview:colorLabel];
        [self addSubview:selectedColorLabel];
        
        
        startPoint += self.imageScroll.frame.size.height + 20;
        [self addSubview:self.imageScroll];
    
    
        NSMutableArray * productSizes = [[NSMutableArray alloc] init];
        
        
        for(KiwiAttribute * attribute in self.product.attribute)
        {
            if([attribute.type isEqualToString:@"Size"])
                [productSizes addObject:attribute];
        }
    
        sizes = [[[NSBundle mainBundle] loadNibNamed:@"ProductSize" owner:nil options:nil] firstObject];
        sizes.delegate = self;

        
        sizes.frame = CGRectMake(0, startPoint, sizes.frame.size.width, 50);
        sizes.sizes = productSizes;
        if([sizes setSizesView])
        {
            [self addSubview:sizes];
            startPoint += 110;
        }
    
    
    
    
    UIView * lastPeice = [[[NSBundle mainBundle] loadNibNamed:@"TopDetails" owner:nil options:nil] objectAtIndex:1];
    _offerDetails = (UILabel*)[lastPeice viewWithTag:3];

    CGRect frame = [Utitlities getLableSizeFromText:self.product.tag.type andLable:_offerDetails];
    
    lastPeice.frame = CGRectMake(0, startPoint, lastPeice.frame.size.width, 87 + frame.size.height);
    
    _offerDetails.numberOfLines = 0;
    _offerDetails.frame = frame;
    _offerDetails.text = self.product.tag.type;
    startPoint += 97 + _offerDetails.frame.size.height;
    
    _addToCart = (UIButton*)[lastPeice viewWithTag:5];
    [_addToCart addTarget:self action:@selector(addItemToCart:) forControlEvents:UIControlEventTouchUpInside];
    
//    _addToCart.layer.shadowColor = [UIColor darkGrayColor].CGColor;
//    _addToCart.layer.shadowOpacity = 0.8;
//    _addToCart.layer.shadowRadius = 2;
//    _addToCart.layer.shadowOffset = CGSizeMake(-1.0f, 0.0f);
//    
    [self addSubview:lastPeice];
   
    
    self.frame = CGRectMake(0, 0, 320, startPoint);
}

#pragma title and labels 

-(void)setupTitles
{
     startPoint = 360;
    NSLog(@"TAG: %@",self.product.tag.type);
    if (self.product.tag.type.length > 0)
    {

        UILabel * availbe = [[UILabel alloc] initWithFrame:CGRectMake(10, startPoint, 200, 25)];
        availbe.text = self.product.tag.type;
        NSLog(@"TAG: %@",self.product.tag.type);
        availbe.textColor = [UIColor whiteColor];
        availbe.numberOfLines = 0;
        [availbe sizeToFit];
        
        availbe.frame = CGRectMake(availbe.frame.origin.x, availbe.frame.origin.y, availbe.frame.size.width + 15, availbe.frame.size.height);
        availbe.textAlignment = NSTextAlignmentCenter;
        
        availbe.backgroundColor =  DEFUALT_COLOR;
        
        
        startPoint += 35;
        self.frame = CGRectMake(0, 0, 320, startPoint);
        [self addSubview:availbe];
    }
    
    
    UILabel * price = [[UILabel alloc] initWithFrame:CGRectMake(10, startPoint, 200, 25)];
    price.text = self.product.price;
    price.font = [UIFont boldSystemFontOfSize:22.0f];
    price.numberOfLines = 0;
    [price sizeToFit];
    startPoint += price.frame.size.height ;
    self.frame = CGRectMake(0, 0, 320, startPoint);
    [self addSubview:price];
    
    
//    
//    UILabel * details = [[UILabel alloc] initWithFrame:CGRectMake(10, startPoint, 200, 25)];
//    details.frame = [Utitlities getLableSizeFromText:self.product.productDetails andLable:details];
//    details.text  = self.product.productDetails;
//    details.numberOfLines = 0;
//    startPoint += details.frame.size.height + 10;
//    self.frame = CGRectMake(0, 0, 320, startPoint);
//    [self addSubview:details];
    
    UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(10, startPoint, 200, 25)];
    title.frame = [Utitlities getLableSizeFromText:self.product.name andLable:title];
    title.text  = self.product.name;
    title.font = [UIFont systemFontOfSize:13.0];
    title.numberOfLines = 0;
    startPoint += title.frame.size.height ;
    self.frame = CGRectMake(0, 0, 320, startPoint);
    [self addSubview:title];
}


#pragma Top images scroller

-(void)setupScrollerImager
{
    self.topImagesScroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 350)];
    self.topImagesScroller.tag = 1;
    self.topImagesScroller.delegate = self;
    self.topImagesScroller.pagingEnabled = YES;
    
    
    self.topImagesScroller.contentSize = CGSizeMake( 320, 350);
    

        UIImageView * image = [self getImageFromUrl:self.product.image.imageUrl andIndex:0
                               ];
        [self.topImagesScroller addSubview:image];
    
    self.frame = CGRectMake(0, 0, 320, 350);
    [self addSubview:self.topImagesScroller];
}

-(UIImageView*)getImageFromUrl:(NSString*)url andIndex:(int)imageIndex
{
    int startX = 320 * imageIndex;
    UIImageView * image = [[UIImageView alloc] initWithFrame:CGRectMake(startX, 0, 320, 380)];
    [image setImageWithURL:[Utitlities imageUrl:url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE]];
    
    return image;
}

//-(void)updateMainImage:(ProductColorsAndSizes*) color
//{
//    UIImageView *image=  [self.topImagesScroller.subviews firstObject];
//    
//    [image setImageWithURL:[NSURL URLWithString:(NSString* )color.colorImageUrl] placeholderImage:[UIImage imageNamed:@"logo120.png"] ];
//    
//}


//-(void)imagePressedAtIndex:(int)index
//{
//    [self updateMainImage:[self.product.productColors objectAtIndex:index]];
//    [sizes updateSizeForColor:[self.product.productColors objectAtIndex:index]];
//}

//-(void) sizeClicked:(int)index
//{
//    
//    NSMutableArray *images  = [[NSMutableArray alloc] init];
//    
//    for (int i = 0; i < self.product.productColors.count; i++)
//    {
//        ProductColorsAndSizes *color = [self.product.productColors objectAtIndex:i];
//        
//        if([color.colorAvailableSizes containsObject:[NSNumber numberWithInt:index]])
//            [images addObject:[NSNumber numberWithInt:i]];
//    }
//    
//    [self.imageScroll updateColorImagesWithAvailableColors:[NSArray arrayWithArray:images]];
//}

-(IBAction)addItemToCart:(id)sender
{
    int page = self.imageScroll.scrollView.contentOffset.x / self.imageScroll.scrollView.frame.size.width;
    
    UIImageView *image =  [[self.topImagesScroller subviews] objectAtIndex:page];
    
    [self startAnimation:image andDestination:CGPointMake(288,35 )];
    
   
    
    [[CartController getsharedInstance] addProduct:self.product];
    
}

-(void)startAnimation:(UIImageView *)image andDestination:(CGPoint)point;
{

    
    UIImageView *copyImage = [[UIImageView alloc] initWithImage:image.image];
    
    UIView* parentView = [KiwiNavigationBarController getCenterViewNavigationController].view;
    
    
    CGRect rectInSuperview = [self convertRect:image.frame toView:parentView];
    
    copyImage.frame = CGRectMake(0, rectInSuperview.origin.y + 5
                                 , image.frame.size.width, image.frame.size.height);
    
    [parentView addSubview:copyImage];
    
    [UIView animateWithDuration:1.0 delay:0.0 options:0
                     animations:^{
                         
                         copyImage.frame = CGRectMake(point.x, point.y
                                                      ,10, 10);
                         
                     }
                     completion:^(BOOL finished)
     {
         [copyImage removeFromSuperview];
     }];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
