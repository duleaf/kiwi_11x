//
//  TopDetails.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٠‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImagesScroll.h"
#import "ProductSize.h"
#import "KiwiProductDetails.h"
@interface TopDetails : UIView <UIScrollViewDelegate,ScrollerImagesDelegate,SizeClicked>


@property (nonatomic,strong) KiwiProductDetails * product;
@property (nonatomic,strong) IBOutlet UIScrollView * topImagesScroller;
@property (nonatomic,strong) ImagesScroll * imageScroll;
@property (nonatomic,strong) UIButton * addToCart;
@property (nonatomic,strong) UILabel * offerDetails;
@property (nonatomic,strong) NSMutableArray *colors;

-(void)setProductDetails:(KiwiProductDetails *)product;
+(int) getH;
-(IBAction)addItemToCart:(id)sender;

@end
