//
//  RegisterationViewController.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٢‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "RegisterationViewController.h"

@interface RegisterationViewController ()
{
    UIActivityIndicatorView *activityView;
}
@end

@implementation RegisterationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.user = [[KiwiUser alloc]init];
    [self.user fillDummyData];
    activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
    [activityView startAnimating];
    activityView.center = self.view.center;
    [self.navigationController.view addSubview:activityView];

    [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(fillData) userInfo:nil repeats:NO];
    
}


-(void)fillData
{
    [activityView removeFromSuperview];
    
    self.fName.text = self.user.fName;
    self.lName.text = self.user.lName;
    self.companyName.text = self.user.companyName;
    self.country.text = self.user.country;
    self.streetAddress.text = self.user.streetAddress;
    self.suit.text = self.user.suit;
    self.town.text = self.user.town;
    self.state.text = self.user.state;
    self.zipCode.text = self.user.zipCode;
    self.phone.text = self.user.phone;
    self.email.text = self.user.email;
    self.phone.text = self.user.phone;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
