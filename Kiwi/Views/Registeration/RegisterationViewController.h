//
//  RegisterationViewController.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٢‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KiwiUser.h";
@interface RegisterationViewController : UITableViewController

@property (nonatomic,strong) KiwiUser * user;
@property (nonatomic,strong) IBOutlet UITextField * fName;
@property (nonatomic,strong) IBOutlet UITextField * lName;
@property (nonatomic,strong) IBOutlet UITextField * companyName;
@property (nonatomic,strong) IBOutlet UITextField * streetAddress;
@property (nonatomic,strong) IBOutlet UITextField * suit;
@property (nonatomic,strong) IBOutlet UITextField * town;
@property (nonatomic,strong) IBOutlet UITextField * state;
@property (nonatomic,strong) IBOutlet UITextField * zipCode;
@property (nonatomic,strong) IBOutlet UITextField * country;
@property (nonatomic,strong) IBOutlet UITextField * phone;
@property (nonatomic,strong) IBOutlet UITextField * email;

-(void)fillData;
@end
