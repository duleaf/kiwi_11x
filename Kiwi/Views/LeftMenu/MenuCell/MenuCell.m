//
//  MenuCell.m
//  SlidingMenu
//
//  Created by Mohammed Salah on ٢٨‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(MenuCell*)convertMeToSecondTypeWithAnim
{
    
    if ((self.img.frame.origin.x != 0 ) && (self.img.frame.origin.y != 0 ))
    {
        self.img.frame = CGRectMake(0,0, 0,0);
        
        [UIView animateWithDuration:0.2 delay:0.0 options:0
                         animations:^{
                             self.title.frame = CGRectMake( self.title.frame.origin.x - 36, self.title.frame.origin.y, self.title.frame.size.width , self.title.frame.size.height);
                         }
                         completion:^(BOOL finished)
         {
//             [_delegate animationOfType:MainMenuCellConverted finishedWithParamters:[NSArray arrayWithObject:indexPath]];
         }];
    }
    else
    {
//        [_delegate animationOfType:MainMenuCellConverted finishedWithParamters:[NSArray arrayWithObject:indexPath]];
        
    }

    
    
    return self;
}


@end
