//
//  MenuCell.h
//  SlidingMenu
//
//  Created by Mohammed Salah on ٢٨‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CellAnimationDelegate <NSObject>

-(void)cellConverted;

@end

@interface MenuCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIButton * clickingButton;
@property (nonatomic,strong) IBOutlet UILabel * title;
@property (nonatomic,strong) IBOutlet UIImageView * img;
@property (nonatomic,strong) IBOutlet UIImageView * seperator;
@property (nonatomic,strong) IBOutlet UIView * fade;
@property (nonatomic,weak) id <CellAnimationDelegate> delegate;



-(MenuCell*)convertMeToSecondTypeWithAnim;

@end
