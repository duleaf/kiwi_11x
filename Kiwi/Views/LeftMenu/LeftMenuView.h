//
//  LeftMenuView.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٥‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftMenuController.h"
#import "MenuCell.h"


@interface LeftMenuView : UIViewController <CellAnimationDelegate>

@property (nonatomic,strong) IBOutlet UITableView * table;

-(void)weeklyAdsLoaded;

@end
