//
//  LeftMenuView.m
//  Kiwi
//
//  Created by Mohammed Salah on ٢٥‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "LeftMenuView.h"
#import "MenuCell.h"
#import "KiwiCategories.h"
#import "WeeklyAdsController.h"

#define FadeOutAnimation 0.1f
#define FadeInAnimation 0.06f
#define SECTION_INDEX 0


@interface LeftMenuView ()
{
    NSMutableArray * menuItems;
    NSArray * icons;
    LeftMenuController * menuController;
    UIScrollView * upperImage;
    UIScrollView * lowerImage;
    UIActivityIndicatorView *activityView;
    UIColor * homeColor;
    WeeklyAdsController * weeklyAdscontroller;
}
@end

@implementation LeftMenuView

@synthesize table;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadIntialMenu];
    
    NSString * path = [[NSBundle mainBundle] pathForResource:@"MenuItems" ofType:@"plist"];
    icons = [NSArray arrayWithContentsOfFile:path];
    upperImage = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    lowerImage = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.table.frame.size.height - 50, 320, 50)];
    self.view.backgroundColor = [UIColor clearColor];
    
    upperImage.backgroundColor =LIGHT_RED_COLOR;
    homeColor = upperImage.backgroundColor;
    
    NSLog(@"Color %@",upperImage.backgroundColor);
    
    lowerImage.backgroundColor = [UIColor darkGrayColor];
    
    
    int startPoint = 60 *(int) menuItems.count + 20;
    
    lowerImage.frame = CGRectMake(0, startPoint, 320, [UIScreen mainScreen].bounds.size.height - startPoint);
    
    self.table.opaque = NO;
    self.table.backgroundView = [UIView new];
    self.table.backgroundColor = [UIColor darkGrayColor];
    [self.table.backgroundView addSubview:upperImage];
    [self.table.backgroundView addSubview:lowerImage];
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    
    
    upperImage.frame = CGRectMake(0, 0, 320, (-1) *scrollView.contentOffset.y);
    
    int startPoint = 60 * menuItems.count + ((-1)*scrollView.contentOffset.y);
    
    lowerImage.frame = CGRectMake(0, startPoint, 320, [UIScreen mainScreen].bounds.size.height - startPoint);
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell %i",(int) indexPath.row];
    ;
    MenuCell *cell = [self.table dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        if(menuController.menuLevel > 0 && indexPath.row != 0 )
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"MenuCell" owner:nil options:nil] objectAtIndex:1];
            [cell setRestorationIdentifier: CellIdentifier];
            cell.alpha = 0.0;

        }
        
        else
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"MenuCell" owner:nil options:nil] firstObject];
            [cell setRestorationIdentifier: CellIdentifier];
            
            cell.alpha = 0.0;
            cell.contentView.backgroundColor = LIGHT_RED_COLOR;
            cell.seperator.backgroundColor = SEP_LIGHT_COLOR;
            [UIView animateWithDuration:FadeInAnimation * (indexPath.row )  delay:0.0 options:0
                             animations:^{
                                 cell.alpha = 1.0;
                             }
                             completion:nil];
            
        }
        
                    
        
    }

    [self handelCellView:cell atIndex:(int)indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void) handelCellView :(MenuCell*) cell atIndex:(int) cellIndex
{
    KiwiCategories * category = [menuItems objectAtIndex:cellIndex];
    NSString *cellTitle = category.name;
    NSString *cellIcon = category.name;
    
    
    
    if(menuController.menuLevel > 0)
    {
       
        
            if(cellIndex == 0)
            {
                cell.title.frame = CGRectMake(32, 20, 180, 21);
                cell.img.frame   = CGRectMake(11, 5, 10, 40);
                cell.title.text = @"Menu";
                cell.img.image = [UIImage imageNamed:@"nav-header-icon-back_ios7.png"];
                cell.contentView.backgroundColor = DARK_RED_COLOR;
                return;
            }
        
           else if(cellIndex < menuController.menuLevel )
            {
                cell.contentView.backgroundColor = DARK_RED_COLOR;
                cell.title.textColor = [UIColor whiteColor];
            }
        
           else if (cellIndex == menuController.menuLevel)
            {
                cell.contentView.backgroundColor = LIGHT_RED_COLOR;
                cell.title.textColor = [UIColor whiteColor];
            }
        
    }
    else
    {
        cell.img.frame   = CGRectMake(1, 1, 46, 48);
        cellIcon = [icons objectAtIndex:cellIndex];
    }
    
    NSLog(@"CellIcon : %@",cellIcon);
    [cell.title setText:cellTitle];
    [cell.img setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",cellIcon]]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    weeklyAdscontroller = nil;
    
    KiwiCategories * targetedCategory = [menuItems objectAtIndex:indexPath.row];
    
    if([targetedCategory.name isEqualToString:@"Weekly Ads"] &&!([WeeklyAdsController getWeeklyAdsProducts]) )
    {
        MenuCell * cell = [self.table cellForRowAtIndexPath:indexPath];
        weeklyAdscontroller = [[WeeklyAdsController alloc] init];
        activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhite];
        activityView.frame = CGRectMake(210, 15, 20, 20);
        [activityView startAnimating];
        [cell addSubview:activityView];
        [weeklyAdscontroller loadProducts];
    }
    
    else if (indexPath.row ==0)
    {
        [self firstCellClicked];
        [self convertFirstCell];
        
    }
    
    
    else
    {
        [self addLevelWithSelectedIndex:indexPath];
        [self convertFirstCell];

    }

//
//    NSString * viewIdentifier = ((Categories*)[menuItems objectAtIndex:indexPath.row]).name;
//    
//    UIViewController * newView = [self.storyboard instantiateViewControllerWithIdentifier:viewIdentifier];
//
//    [KiwiNavigationBarController replaceCenterView:newView];
    
}

-(void) AnimateTableCells
{
    int startPoint = 0;
    
    if(menuController.menuLevel >1)
        startPoint = menuController.menuLevel;
    
    for ( int cellIndex = startPoint ; cellIndex < menuItems.count; cellIndex++)
    {
        if (cellIndex == menuController.menuLevel)
            continue;
        
        else
        {
             MenuCell * cell =(MenuCell *) [table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:cellIndex inSection:SECTION_INDEX]];
            cell.alpha = 0.0;
            
            if(cellIndex == 0)
                [self convertFirstCell];
            
            [UIView animateWithDuration:FadeInAnimation * (cellIndex )  delay:0.0 options:0
                             animations:^{
                                 cell.alpha = 1.0;
                             }
                             completion:nil];
        }
    }

    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(checkLastIndex) userInfo:nil repeats:NO];
}


-(void)addLevelWithSelectedIndex:(NSIndexPath*) indexPath
{
    
    
    if (indexPath.row > menuController.menuLevel)
        menuController.menuLevel ++;
    
    if (indexPath.row < menuController.menuLevel) {
        menuController.menuLevel = (int)indexPath.row;
    }
    
    
    [self FadeOutCellsFromCellAtIndex:indexPath];
    
}


-(void)FadeOutCellsFromCellAtIndex:(NSIndexPath*) indexPath {
    int numberOfcategories = (int)menuItems.count;
    int numberOfdeletedCells =  numberOfcategories - menuController.menuLevel;
    
    
    MenuCell * cell =(MenuCell *) [table cellForRowAtIndexPath:indexPath];
    cell.restorationIdentifier = [NSString stringWithFormat:@"Cell %i",menuController.menuLevel -1];
    
    [cell convertMeToSecondTypeWithAnim];
    
    NSIndexPath * targetIndex = [NSIndexPath indexPathForItem:menuController.menuLevel inSection:SECTION_INDEX];
    
    [self.table moveRowAtIndexPath:indexPath toIndexPath:targetIndex];

    NSLog(@"AT: %li  To: %li",(long)indexPath.row , (long)targetIndex.row);
    KiwiCategories * targetedCategory = [menuItems objectAtIndex:indexPath.row];
    [menuItems replaceObjectAtIndex:menuController.menuLevel withObject:targetedCategory];
    

                         
                         int startCell = menuController.menuLevel ;
                         //fade out the upper cell
                         for(int cellIndex = startCell ; cellIndex < numberOfdeletedCells ; cellIndex ++)
                         {
                             
                             
                             [UIView animateWithDuration:FadeOutAnimation *cellIndex  delay:0.0 options:0
                                              animations:^
                             {
                                 int newIndex = menuItems.count + menuController.menuLevel - cellIndex -1;
                                 MenuCell * cell =(MenuCell *) [table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:newIndex inSection:SECTION_INDEX]];
                                 
                                 
                                 cell.alpha = 0.0 ;
                                 
                             }
                             completion:^(BOOL finished){
                             
                                
                                 
                             }];
                             
                     }
    
    [NSTimer scheduledTimerWithTimeInterval:(FadeOutAnimation ) target:self selector:@selector(insertNewLevelCells) userInfo:nil repeats:NO];
                     
//                         
//                             // need to fade upper part of selected cell
//                             if(endIndex == numberOfcategories )
//                             {
//                                 MenuCell * cell =(MenuCell *) [table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:menuItems.count -1 inSection:SECTION_INDEX]];
//                                 
//                                 
//                                 [cell convertMeToSecondTypeWithAnim];
//                                 
//                                int newStartIndex = menuController.menuLevel ;
//                                int newEndIndex   = startIndex - 1;
//                                 
//                                 [self FadeOutCellsFromCellAtIndex:newStartIndex andLastCellIndex:newEndIndex];
//                             }
//                             
//                             else
//                             {
//                             }
     
                         
}


-(void) insertNewLevelCells
{
    KiwiCategories * selectedCategory = [menuItems objectAtIndex: menuController.menuLevel];
    
    NSArray * newCategories = [menuController getMenuItemsWithParentId:selectedCategory.cat_id];
    
    int cellIndex = menuController.menuLevel +1;
    

    while ( menuItems.count > menuController.menuLevel +1 )
    {
        
        [menuItems removeLastObject];
        int lastCellIndex = (int) menuItems.count ;
        
        
        NSIndexPath * cellIndexPath = [NSIndexPath indexPathForRow:lastCellIndex inSection:SECTION_INDEX];
        
        [self.table deleteRowsAtIndexPaths:[NSArray arrayWithObject:cellIndexPath] withRowAnimation:SECTION_INDEX];
    }
    

    
    
    for (KiwiCategories * category in newCategories)
    {
        [menuItems addObject:category];
        
        NSIndexPath * cellIndexPath = [NSIndexPath indexPathForRow:cellIndex inSection:SECTION_INDEX];
        
        [table insertRowsAtIndexPaths:[NSArray arrayWithObject:cellIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        
        cellIndex ++;
    }
    
    
    for(int cellIndex = 0 ; cellIndex <= menuController.menuLevel  ; cellIndex ++)
    {
        MenuCell * cell =(MenuCell *) [table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:cellIndex inSection:SECTION_INDEX]];
        
        
        [UIView animateWithDuration:FadeInAnimation * (cellIndex )  delay:0.0 options:0
                         animations:^{
                             cell.seperator.backgroundColor = SEP_DARK_RED_COLOR;
                             cell.contentView.backgroundColor = DARK_RED_COLOR;
                             cell.title.textColor = [UIColor whiteColor];
                             
                             if(cellIndex == menuController.menuLevel)
                             {
                                 cell.contentView.backgroundColor = LIGHT_RED_COLOR;
                                 
                             }
                         }
                         completion:nil];
        

    }
    
    [menuController itemPressedWithCategory:[menuItems objectAtIndex:menuController.menuLevel]];

    [self AnimateTableCells];
    
}


-(void)convertFirstCell
{
    MenuCell * firstCell = (MenuCell *)[table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    if (menuController.menuLevel > 0)
    {
        upperImage.backgroundColor = DARK_RED_COLOR;
        firstCell.title.frame = CGRectMake(40, 14, 182, 21);
        firstCell.img.frame   = CGRectMake(1, 1 , 46, 48);
        firstCell.title.text = @"Menu";
        firstCell.img.image = [UIImage imageNamed:@"nav-header-icon-back_ios7.png"];
        
        //        Menu_Category * cat = [menuItems objectAtIndex:0];
        //        cat.name = @"Menu";
    }
    else
    {
        firstCell.title.frame = CGRectMake(50, 14, 182, 21);
        firstCell.img.frame   = CGRectMake(1, 1, 42, 42);
        firstCell.title.text = @"Home";
        firstCell.img.image = [UIImage imageNamed:[icons firstObject]];
        upperImage.backgroundColor = homeColor;
        [menuController itemPressedWithCategory:[menuItems objectAtIndex:menuController.menuLevel]];

        //        [menuItems replaceObjectAtIndex:0 withObject:@"Home"];
        //        Menu_Category * cat = [menuItems objectAtIndex:0];
        //        cat.name = @"Home";
    }
    
    
    
}

-(void) firstCellClicked
{
    if (menuController.menuLevel > 0)
    {
//        [[UINavigationBar appearance] setBarTintColor:DEFUALT_COLOR];
        
        menuController.menuLevel = 0;
        
        menuItems = nil;
        
        [self loadIntialMenu];
        
        
        
        [table reloadData];
    }
}

-(void)loadIntialMenu
{
    menuController = [[LeftMenuController alloc] init];
    //Top menu item has parent id "0"
    menuItems = [NSMutableArray arrayWithArray:[menuController getMenuItemsWithParentId:@"0"]];
}

-(void)checkLastIndex
{
    if(menuController.menuLevel == 0)
        return;
    
    
    KiwiCategories * cat = [menuItems objectAtIndex:menuController.menuLevel];
    NSArray * items = [menuController getMenuItemsWithParentId:cat.cat_id];
    
    if (items.count < 1)
    {
        [self.sidePanelController showCenterPanelAnimated:YES];
    }
    
}

-(void)weeklyAdsLoaded
{
    if(weeklyAdscontroller)
    {
        NSOperationQueue *myQueue = [NSOperationQueue mainQueue];
        [myQueue addOperationWithBlock:^{
            [activityView removeFromSuperview];
            [self addLevelWithSelectedIndex:[NSIndexPath indexPathForItem:2 inSection:0]];
            [self convertFirstCell];
        }];
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
