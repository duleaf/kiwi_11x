//
//  DetialsView.h
//  Kiwi
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KiwiAttribute.h"
@interface DetialsView : UIView

@property (nonatomic,strong) NSArray * attributes;
@property (nonatomic,strong) IBOutlet UIView * additionaInfo;
@property (nonatomic,strong) IBOutlet UIView * returnPolicy;
@property (nonatomic,strong) IBOutlet UIView * returnPolicyDetails;
@property (nonatomic,strong) IBOutlet UIButton * showMore;


-(void)loadAttributes;
-(int) getHieght;
@end
