//
//  DetialsView.m
//  Kiwi
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "DetialsView.h"

@implementation DetialsView

static int startPoint;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(int) getHieght
{
    return startPoint;
}

-(void)loadAttributes
{
    startPoint = 30;
    
    
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ LIKE primiry",[KiwiAttribute new].detials ];
//    NSArray * primaryAtts = [self.attributes filteredArrayUsingPredicate:predicate ];
    self.additionaInfo.autoresizesSubviews = NO;
    self.returnPolicyDetails.autoresizesSubviews = NO;
    self.returnPolicy.autoresizesSubviews = NO;
    
    self.additionaInfo.autoresizingMask = UIViewAutoresizingNone;
    self.returnPolicyDetails.autoresizingMask = UIViewAutoresizingNone;
    self.returnPolicy.autoresizingMask = UIViewAutoresizingNone;
    
    
    
    for(KiwiAttribute * attribute in self.attributes)
    {
        if([attribute.detials isEqualToString:@"primiry"])
        {
            UILabel *attributeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, startPoint, 100, 25)];
            
            attributeLabel.text = [NSString stringWithFormat:@"%@: %@",attribute.type , attribute.value];
            attributeLabel.font = [UIFont systemFontOfSize:13.0f];
            [attributeLabel sizeToFit];
            attributeLabel.frame = [Utitlities getLableSizeFromText:attributeLabel.text andLable:attributeLabel];
            
            [self.additionaInfo addSubview:attributeLabel];
            
            startPoint += attributeLabel.frame.size.height ;
            
        }

        
    }
    
    
    self.showMore.frame = CGRectMake(self.showMore.frame.origin.x
                                     , startPoint, self.showMore.frame.size.width, self.showMore.frame.size.height);
    startPoint += self.showMore.frame.size.height + 10;
    self.additionaInfo.frame = CGRectMake(self.additionaInfo.frame.origin.x
                                          , self.additionaInfo.frame.origin.y
                                          , self.additionaInfo.frame.size.width, startPoint);

    startPoint +=30;
    
    self.returnPolicy.frame = CGRectMake(self.returnPolicy.frame.origin.x
                                     , startPoint, self.returnPolicy.frame.size.width, self.returnPolicy.frame.size.height);
    startPoint += self.returnPolicy.frame.size.height + 15;
    
    

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
