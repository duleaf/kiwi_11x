//
//  ProductDetailsViewController.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٠‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "ProductDetailsViewController.h"
#import "KiwiProductDetails.h"
//#import "ProductColorsAndSizes.h"
#import "ImagesScroll.h"
@interface ProductDetailsViewController ()
{
    ItemDetails * currentDetials;
    UITableViewCell * detailsCell;
}
@end

@implementation ProductDetailsViewController

@synthesize tabel,cellsHeight;

static NSArray * views;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.product.price = [NSString stringWithFormat:@"AED %.02f",[Utitlities getRundomNumber:200 andMin:10 isFloat:YES]];
 
    _topView = [[TopDetails alloc] init];
    [_topView setProductDetails:self.product];

    self.itemDetails = [[[NSBundle mainBundle] loadNibNamed:@"ItemDetails" owner:nil options:nil] firstObject];
    self.itemDetails.frame = CGRectMake(0, 45, self.itemDetails.frame.size.width,200);
    self.itemDetails.attribute = self.product.attribute;
    [self.itemDetails loadAttributes];
    currentDetials = self.itemDetails;
    
    self.returnPolicy = [[ItemDetails alloc] init];
    self.offers = [[ItemDetails alloc] init];
    
    self.itemDetails.attribute = self.product.attribute;
    currentDetials = self.itemDetails;
    
//    self.detialsView = [[[NSBundle mainBundle] loadNibNamed:@"DetialsView" owner:nil options:nil] firstObject];
//    self.detialsView.attributes = self.product.attribute;
//    [self.detialsView loadAttributes];
    
    views = [[NSArray alloc] initWithObjects:@"image",@"title",@"recommended", nil];
    cellsHeight = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:[TopDetails getH]],[NSNumber numberWithInt:250],[NSNumber numberWithInt:180], nil];
    
    NSLog(@"H: %i",[self.detialsView getHieght]);
    tabel.frame = CGRectMake(0, 0, 320, self.view.frame.size.height);
    
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)ItemSelected:(id)sender
{
    [currentDetials removeFromSuperview];
    
    UIButton * button = (UIButton*) sender;
    
    if (button.tag == 1)
    {
        self.itemDetails = [[[NSBundle mainBundle] loadNibNamed:@"ItemDetails" owner:nil options:nil] firstObject];
        self.itemDetails.frame = CGRectMake(0, 45, self.itemDetails.frame.size.width,200);
        
        self.itemDetails.attribute = self.product.attribute;
        [self.itemDetails loadAttributes];
        [cellsHeight replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:245]];
        currentDetials = self.itemDetails;
    }
    else if(button.tag == 2)
    {
        
        self.offers = [[[NSBundle mainBundle] loadNibNamed:@"ItemDetails" owner:nil options:nil] firstObject];
        
        self.offers.frame = CGRectMake(0, 45, self.offers.frame.size.width,200);
        
        [self.offers loadText:@"" anddTitle:@"Offers"];
        [cellsHeight replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:245]];
        currentDetials = self.offers;
    }
    else if (button.tag == 3)
    {
        self.returnPolicy = [[[NSBundle mainBundle] loadNibNamed:@"ItemDetails" owner:nil options:nil] firstObject];
        
        self.returnPolicy.frame = CGRectMake(0, 45, self.returnPolicy.frame.size.width, 200);
        
        [self.returnPolicy loadText:@"" anddTitle:@"Return Policy"];
        [cellsHeight replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:245]];
        currentDetials = self.returnPolicy;
    }
    
    [self.tabel reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [views objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tabel dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProductDetailsViewController" owner:nil options:nil] objectAtIndex:indexPath.row];
    }
 
    if(indexPath.row == 0)
    {
        [cell addSubview:_topView];
        cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width);
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self HandleCellData:cell atIndex:indexPath];
    return cell;
}

-(void)HandleCellData:(UITableViewCell*)cell atIndex:(NSIndexPath*) indexPath
{

    if ((int)indexPath.row == 1)
    {
        UIView * tempView = [cell viewWithTag:5];
        tempView.autoresizesSubviews = NO;
        tempView.autoresizingMask = UIViewAutoresizingNone;
        
        
        UIButton * button = (UIButton*)[tempView viewWithTag:1];
        
        [button addTarget:self action:@selector(ItemSelected:) forControlEvents:UIControlEventTouchUpInside];
       
        button = (UIButton*)[tempView viewWithTag:2];
        
        [button addTarget:self action:@selector(ItemSelected:) forControlEvents:UIControlEventTouchUpInside];
        
        button = (UIButton*)[tempView viewWithTag:3];
        
        [button addTarget:self action:@selector(ItemSelected:) forControlEvents:UIControlEventTouchUpInside];
        
        [currentDetials removeFromSuperview];
        
        currentDetials .autoresizingMask = UIViewAutoresizingNone;
        currentDetials.frame = CGRectMake(0, 45, 320, 200);

        [cell addSubview:currentDetials];

    }

    
    else if ((int)indexPath.row == 2)
    {
        [cell addSubview:[self getRecommendedView]];
        
        UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 200, 20)];
        title.text = @"recommended for you";
        title.font = [UIFont boldSystemFontOfSize:15];
        
        [cell addSubview:title];
    }
    NSLog(@"Index : %i",(int)indexPath.row);
}

-(void)manageDropDownMenusForCell:(UITableViewCell*)cell
{
    
}

-(IBAction)addProductToCart:(id)sender
{
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber * num = [cellsHeight objectAtIndex:indexPath.row];
    return num.integerValue;
}


-(KiwiProductDetails*)getproduct
{
    KiwiProductDetails * product = [[KiwiProductDetails alloc] init];

    product.price = self.product.price;
    product.name = self.product.name;
    
//    product.productSpecialOffer = @"Spend $99 save 20% on Men's, Women's and Kids' clothing, shoes and accessories. Offer valid on Regular priced items only. Offer available online only. Discount applied at Checkout.";
    
//    ProductColorsAndSizes * color1 = [[ProductColorsAndSizes alloc] init];
//    color1.colorImageUrl =@"http://scene7.targetimg1.com/is/image/Target/15690866?wid=410&hei=410" ;
//    color1.colorAvailableSizes = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:1],[NSNumber numberWithInt:2],[NSNumber numberWithInt:3],[NSNumber numberWithInt:0], nil];
//    
    
//    ProductColorsAndSizes * color2 = [[ProductColorsAndSizes alloc] init];
//    color2.colorImageUrl = @"http://scene7.targetimg1.com/is/image/Target/15690860?wid=410&hei=410";
//    color2.colorAvailableSizes = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:1],[NSNumber numberWithInt:2],[NSNumber numberWithInt:3],[NSNumber numberWithInt:5], nil];
//    
    
//    product.images = [NSArray arrayWithObjects:@"http://scene7.targetimg1.com/is/image/Target/15826712?wid=410&hei=410",@"http://scene7.targetimg1.com/is/image/Target/15826712_Alt01?wid=410&hei=410", nil];
//    product.productColors = [NSArray arrayWithObjects:color1,color2, nil];
    

    return product;
}


-(UIView*)getRecommendedView
{
    imageScroll = [[[NSBundle mainBundle] loadNibNamed:@"ImagesScroll" owner:nil options:nil] firstObject];
    imageScroll.delegate = self;
    
    //adding samples
    
    imageScroll.images = [[NSMutableArray alloc] init];
    for(KiwiProductDetails * pro in self.recomendedProducts)
    {
        
        [imageScroll.images addObject: pro.image.imageUrl];
    }
    
    
    [imageScroll buildScrollWithType:RecommenedImageView andImages:imageScroll.images WithFrame:CGRectMake(0, 30, 320, 130)];
//    imageScroll.frame = CGRectMake(0, 10, imageScroll.scrollView.frame.size.width
//                                         , imageScroll.scrollView.frame.size.height);
    
    return imageScroll;
}

-(void)imagePressedAtIndex:(int)index
{
    ProductDetailsViewController * prodetails = [self.storyboard instantiateViewControllerWithIdentifier:@"proDetails"];
    KiwiProductDetails * selectedProduct = [self.recomendedProducts objectAtIndex:index];
    
    prodetails.product = selectedProduct;
    [self.recomendedProducts addObject:self.product];
    [self.recomendedProducts removeObjectAtIndex:index];
    
    prodetails.recomendedProducts = self.recomendedProducts;
    [self.navigationController pushViewController:prodetails animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
