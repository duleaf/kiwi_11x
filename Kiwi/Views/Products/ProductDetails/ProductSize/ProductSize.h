//
//  ProductSize.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٠‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SizeClicked <NSObject>

-(void) sizeClicked:(int)index;

@end

@interface ProductSize : UIView

@property (nonatomic,strong)NSMutableArray *sizes;
@property (nonatomic,weak) id <SizeClicked> delegate;

//-(void) updateSizeForColor:(ProductColorsAndSizes*)color;

-(BOOL) setSizesView;
@end
