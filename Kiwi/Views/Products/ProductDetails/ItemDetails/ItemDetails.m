//
//  ItemDetails.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٣‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "ItemDetails.h"
#import  "ProductDetailsViewController.h"
#define WebH 175
@implementation ItemDetails

bool expanded;
int height;
int mainH;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)loadText:(NSString*)text anddTitle:(NSString*)titleTxt
{
    self.frame = CGRectMake(self.frame.origin.x, 45, self.frame.size.width, 200);

    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"about.html" ofType:nil]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
    height = self.webView.scrollView.contentSize.height;
    NSLog(@"%f",self.webView.scrollView.contentSize.height);
    self.webView.scrollView.scrollEnabled = NO;

    self.webView.frame = CGRectMake(0, 0, 320, WebH);
    expanded = false;
//
//    UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(15, height, 20, 30)];
//    title.text = titleTxt ;
//    title.textColor = [UIColor darkGrayColor];
//    title.font = [UIFont boldSystemFontOfSize:15];
//    [title sizeToFit];
//    
//    
//    UILabel * value = [[UILabel alloc] initWithFrame:CGRectMake(15, height + 35 , 20, 30)];
//    value.text = text ;
//    value.frame = [Utitlities getLableSizeFromText:text andLable:value];
//    value.textColor = [UIColor darkGrayColor];
//    value.font = [UIFont boldSystemFontOfSize:13];
//    
//    [self addSubview:title];
//    [self addSubview:value];
//    
//    mainH = 200;
//    height += value.frame.size.height;
//    
//    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, mainH);
    [self.webView reload];
    
}
-(void) loadAttributes
{
    height = 10;
    self.frame = CGRectMake(self.frame.origin.x, 45, self.frame.size.width, 200);

    
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"about.html" ofType:nil]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
    height = self.webView.scrollView.contentSize.height;
    NSLog(@"%f",self.webView.scrollView.contentSize.height);
    self.webView.scrollView.scrollEnabled = NO;
    expanded = false;
    
//    NSMutableArray * secAttributes = [[NSMutableArray alloc] init];
//    
//    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 2000);
//
//    self.autoresizingMask = UIViewAutoresizingNone;
//    
//    for(KiwiAttribute * attribute in self.attribute)
//    {
//        
//        if(([attribute.detials isEqualToString:@"Color"] || [attribute.detials isEqualToString:@"Size"]))
//            continue;
//        
//        if ([attribute.detials isEqualToString:@"primiry"] )
//        {
//            
//            
//            UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(20, height, 280, 30)];
//            if(height > 200)
//                title.hidden = YES;
//            
//            title.text = attribute.type ;
//            title.textColor = [UIColor darkGrayColor];
//            title.font = [UIFont boldSystemFontOfSize:15];
//            
//            height +=  title.frame.size.height;
//            
//            UILabel * value = [[UILabel alloc] initWithFrame:CGRectMake(20, height , 280, 20)];
//            if(height > 200)
//                value.hidden = YES;
//            
//            value.text = attribute.value ;
//            NSLog(@"Att:%@",attribute.value);
//            value.frame = [Utitlities getLableSizeFromText:attribute.value andLable:value];
//            value.textColor = [UIColor darkGrayColor];
//            value.font = [UIFont systemFontOfSize:13];
//            
//            
//            
//            [self addSubview:title];
//            [self addSubview:value];
//            
//            height += 10 + value.frame.size.height;
//            
//        }
//        else
//        {
//            [secAttributes addObject:attribute];
//        }
//    }
//    
//    mainH = height;
//    
//
//    for(KiwiAttribute * attribute in secAttributes)
//    {
//        
//            UILabel * title = [[UILabel alloc] initWithFrame:CGRectMake(20, height, 280, 30)];
//        if(height > 200)
//            title.hidden = YES;
//            title.text = attribute.type ;
//            title.textColor = [UIColor darkGrayColor];
//            title.font = [UIFont boldSystemFontOfSize:15];
//            [title sizeToFit];
//        
//        height +=  title.frame.size.height;
//            
//            UILabel * value = [[UILabel alloc] initWithFrame:CGRectMake(20, height , 280, 20)];
//        if(height > 200)
//            value.hidden = YES;
//            value.text = attribute.value ;
//            value.frame = [Utitlities getLableSizeFromText:attribute.value andLable:value];
//            value.textColor = [UIColor darkGrayColor];
//            value.font = [UIFont systemFontOfSize:13];
//        
//        [self addSubview:title];
//        [self addSubview:value];
//        
//            height += 10 + value.frame.size.height;
//   
//    }
//    
//    
//    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, 200);

    [self.webView reload];
}


-(IBAction)moreButtonPressed:(id)sender
{
expanded = !expanded;
    
    ProductDetailsViewController * prod = [[KiwiNavigationBarController getCenterViewNavigationController].viewControllers objectAtIndex:1];
    


    if (expanded)
    {
        [prod.cellsHeight replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt: height + 75]];

        [prod.tabel reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        
        self.frame = CGRectMake(self.frame.origin.x, 45, self.frame.size.width, height + 25);
        self.webView.frame = CGRectMake(0, 0, self.frame.size.width, height );
        
//        for (UIView * v in self.subviews)
//        {
//            v.hidden = NO;
//        }
    }
    else
    {
        [prod.cellsHeight replaceObjectAtIndex:1 withObject:[NSNumber numberWithInt:245]];
        
        
        [prod.tabel reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        self.frame = CGRectMake(self.frame.origin.x, 45, self.frame.size.width, 200);
        self.webView.frame = CGRectMake(0, 0, self.frame.size.width, WebH);

//        for (UIView * v in self.subviews)
//        {
//            if(v.frame.origin.y > 200)
//                v.hidden = YES;
//        }
        
    }
    
    
    
}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    CGRect frame = aWebView.frame;
//    frame.size.height = 1;
//    aWebView.frame = frame;
     height = [[aWebView stringByEvaluatingJavaScriptFromString:@"document.height"] floatValue];

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
