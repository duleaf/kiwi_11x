//
//  ItemDetails.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٣‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KiwiAttribute.h"
@interface ItemDetails : UIView

@property (nonatomic,strong) NSArray * attribute;
@property (nonatomic,strong) NSString * returnProlicy;
@property (nonatomic,strong) NSString * offers;
@property (nonatomic,strong) IBOutlet UIWebView * webView;


-(IBAction)moreButtonPressed:(id)sender;
-(void)loadText:(NSString*)text anddTitle:(NSString*)titleTxt;
-(void) loadAttributes;


@end
