//
//  ImagesScroll.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٠‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>



typedef enum : NSUInteger {
    ColorImageViewer ,
    RecommenedImageView
    
} ImageScrollerType;



//delegate to tell view which item selected
@protocol ScrollerImagesDelegate <NSObject>

-(void)imagePressedAtIndex:(int)index;

@end

@interface ImagesScroll : UIView

@property (nonatomic,strong) IBOutlet UIScrollView * scrollView;
@property (nonatomic,weak) id <ScrollerImagesDelegate> delegate;
@property (nonatomic,strong) NSMutableArray *buttons;
@property (nonatomic,strong) NSMutableArray *images;


-(void)buildScrollWithType:(ImageScrollerType)type andImages:(NSArray*)images WithFrame:(CGRect)frame;
-(IBAction)selectedButton:(id)sender;
-(void)updateColorImagesWithAvailableColors:(NSArray*)images;


@end
