//
//  ImagesScroll.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٠‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "ImagesScroll.h"
#import <QuartzCore/QuartzCore.h>
@implementation ImagesScroll

#define COLOR_IMAGE_HEIGHT 50
#define COLOR_IMAGE_WIDTH 40

#define RECOMMENDED_IMAGE_HEIGHT 100
#define RECOMMENDED_IMAGE_WIDTH 85

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)buildScrollWithType:(ImageScrollerType)type andImages:(NSArray*)images  WithFrame:(CGRect)frame
{
    _buttons = [[NSMutableArray alloc] init];
 
    if (type == ColorImageViewer)
    {
        [self setupScrollOfColorTypeWithFrame:frame];
    }
    
    else
    {
        [self setupScrollOfRcommendeTypeWithFrame:frame];
    }
    
}


-(void)setupScrollOfRcommendeTypeWithFrame:(CGRect)frame
{
    
    
    self.frame = frame ;//CGRectMake(0, 30, 320, 130);
    self.scrollView.frame = CGRectMake(0, 15, 320, 100);

    UIImageView * background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 130)];
    [self insertSubview:background belowSubview:self.scrollView];
    background.image = [UIImage imageNamed:@"back_pro_detals.png"];
    int space = 10;
    
    for (int i = 0;  i < self.images.count; i++)
    {
        
        
        UIButton * imageButton = [[UIButton alloc] initWithFrame:CGRectMake((i*(space +RECOMMENDED_IMAGE_WIDTH ))+ space, 0, RECOMMENDED_IMAGE_WIDTH, RECOMMENDED_IMAGE_HEIGHT)];
        UIImageView * img = [[UIImageView alloc] init];
        

    
        
        [img setImageWithURL:[NSURL URLWithString:(NSString* )[self.images objectAtIndex:i]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
         {
             [imageButton setImage:image forState:UIControlStateNormal];
             
         }];
        imageButton.tag = i;
        [imageButton setImage:img.image forState:UIControlStateNormal];

        [imageButton addTarget:self action:@selector(selectedButton:) forControlEvents:UIControlEventTouchUpInside];
        
        imageButton.imageView.layer.cornerRadius = 2;
        imageButton.imageView.clipsToBounds = YES;
        imageButton.backgroundColor = [UIColor clearColor];
        imageButton.layer.shadowColor = [UIColor darkGrayColor].CGColor;
        imageButton.layer.shadowOpacity = 0.8;
        imageButton.layer.shadowRadius = 2;
        imageButton.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
        
        [_buttons addObject:imageButton];
        [self.scrollView addSubview:imageButton];
    }
    
    self.scrollView.contentSize = CGSizeMake(( self.images.count *(RECOMMENDED_IMAGE_WIDTH + space)), self.scrollView.frame.size.height);
}




-(void)setupScrollOfColorTypeWithFrame:(CGRect)frame
{
    
    self.frame = frame;//CGRectMake(10, 10, 300, 90);
    
    self.scrollView.frame = CGRectMake(10, 10, 300, 70);
    self.scrollView.layer.cornerRadius = 2;
    self.scrollView.clipsToBounds = YES;
    
    [self.scrollView.layer setBorderColor: [[UIColor groupTableViewBackgroundColor] CGColor]];
    [self.scrollView.layer setBorderWidth: 1.0];
    int space = 10;
    
    self.scrollView.backgroundColor = [UIColor whiteColor];
    
    
    for (int i = 0;  i < self.images.count; i++)
    {
        
        
        UIButton * imageButton = [[UIButton alloc] initWithFrame:CGRectMake((i*(space + 50))+ space, 10, COLOR_IMAGE_WIDTH, COLOR_IMAGE_HEIGHT)];
        
        imageButton.layer.cornerRadius = 2;
        imageButton.clipsToBounds = YES;
        
        [imageButton.layer setBorderColor: [[UIColor groupTableViewBackgroundColor] CGColor]];
        [imageButton.layer setBorderWidth: 1.0];
        
        UIImageView * img = [[UIImageView alloc] init];
        
        NSString *imageUrl = [self.images objectAtIndex:i];
        
        
        
        [img setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@"logo120.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
         {
             [imageButton setImage:image forState:UIControlStateNormal];

         }];
        imageButton.tag = i;
        [imageButton setImage:img.image forState:UIControlStateNormal];

        [imageButton addTarget:self action:@selector(selectedButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [_buttons addObject:imageButton];
        [self.scrollView addSubview:imageButton];
    }
    
      self.scrollView.contentSize = CGSizeMake(( self.images.count *(ColorImageViewer + space)), self.scrollView.frame.size.height);
}

-(IBAction)selectedButton:(id)sender
{
    UIButton * button = (UIButton*)sender;
    
    [self.delegate imagePressedAtIndex:(int)button.tag];
    
}
//
//-(void)updateColorImagesWithAvailableColors:(NSArray*)images
//{
//    for (int i = 0;  i < _buttons.count;  i++)
//    {
//        if ([images containsObject:[NSNumber numberWithInt:i]]) {
//            
//            UIButton * imageButton = [_buttons objectAtIndex:i];
//            UIImageView * img = [[UIImageView alloc] init];
//            
//            ProductColorsAndSizes *colors = [self.images objectAtIndex:i];
//            
//            [img setImageWithURL:[NSURL URLWithString:(NSString* )colors.colorImageUrl] placeholderImage:[UIImage imageNamed:@"logo120.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
//             {
//                 imageButton.backgroundColor = [UIColor clearColor];
//                 [imageButton setImage:image forState:UIControlStateNormal];
//                 
//             }];
//        }
//        
//        else
//        {
//            UIButton * imageButton = [_buttons objectAtIndex:i];
//            imageButton.backgroundColor = [UIColor grayColor];
//            imageButton.imageView.image = nil;
//        }
//    }
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
