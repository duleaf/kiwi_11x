//
//  ProductDetailsViewController.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٠‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KiwiProduct.h"
#import "ImagesScroll.h"
#import "KiwiProductDetails.h"
#import "DetialsView.h"
#import "TopDetails.h"
#import "ItemDetails/ItemDetails.h"
@interface ProductDetailsViewController : UIViewController
{
    ImagesScroll * imageScroll;
}
@property (nonatomic,strong) IBOutlet UITableView * tabel;
@property (nonatomic,strong) KiwiProductDetails * product;
@property (nonatomic,strong) UIButton * addToCart;
@property (nonatomic,strong) DetialsView * detialsView;
@property (nonatomic,strong) ItemDetails * itemDetails;
@property (nonatomic,strong) ItemDetails * offers;
@property (nonatomic,strong) ItemDetails * returnPolicy;
@property (nonatomic,strong) NSMutableArray * recomendedProducts;
@property (nonatomic,strong) TopDetails * topView;
@property (nonatomic,strong) NSMutableArray *cellsHeight;

-(IBAction)ItemSelected:(id)sender;

@end
