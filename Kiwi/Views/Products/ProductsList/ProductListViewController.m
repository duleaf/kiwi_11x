//
//  ProductListViewController.m
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductCell.h"
#import "KiwiProductThumbnail.h"
#import "ProductDetailsViewController.h"
#import "CartController.h"
#import "KiwiAttribute.h"
#import "FilterViewController.h"
#define NumberOfRecommneded 10
#define NumberOfProductsInEachPage 20

@interface ProductListViewController ()
{
    ProductsController * prodcutController;
    UIActivityIndicatorView *activityView;
    FilterViewController * filter;
    UINavigationController * filterNav ;
    UILabel * noData;
    float HeightestPrice;
    UILabel * loading;
}
@end

@implementation ProductListViewController

@synthesize table;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesture:)];
    [self.filterView addGestureRecognizer:singleTapGestureRecognizer];
    
    prodcutController = [[ProductsController alloc] init];
    prodcutController.delegate = self;
    activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
    
    prodcutController.delegate = self;
    
    self.contentUnAvailable.hidden = NO;
    [activityView startAnimating];
    activityView.center = self.navigationController.view.center;
    [self.contentUnAvailable addSubview:activityView];
    activityView.center = self.navigationController.view.center;
    activityView.frame = CGRectMake(120, activityView.frame.origin.y
                                    , activityView.frame.size.width, activityView.frame.size.height);
    loading = [[UILabel alloc] init];
    loading.frame = CGRectMake(145, activityView.frame.origin.y - 5, 80, 30);
    loading.text= @"Loading...";
    loading.font = [UIFont boldSystemFontOfSize:14.0];
    loading.textColor = [UIColor grayColor];
    [self.contentUnAvailable addSubview:loading];
    
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
       [self reloadProducts];
    }];
    
    
    // Do any additional setup after loading the view.
}


-(IBAction)handleSingleTapGesture:(id)sender
{
    
    
    filter = [self.storyboard instantiateViewControllerWithIdentifier:@"filter"];
    filter.category = self.currentCategory;
    filter.bluredImage = [[UIImageView alloc] init];
//    [self presentViewController:filter animated:YES completion:nil];
    filterNav = [[UINavigationController alloc] initWithRootViewController:filter];
    
    filter.HightesPrice = HeightestPrice;
    
    int screenH = [UIScreen mainScreen].bounds.size.height;
    self.blurView = [[UIERealTimeBlurView alloc] initWithFrame:CGRectMake(0, 0, 320, screenH)];

//    filter.filterTitle = [NSString stringWithFormat:@"%@ in %@",self.numberOfitems.text,self.currentCategory.name];
    [self.blurView addSubview:filterNav.view];
    [self.navigationController.view addSubview:self.blurView];
    self.blurView.hidden = YES;

    
    [self showFilter];
}

-(void)showFilter
{
    int screenH = [UIScreen mainScreen].bounds.size.height;
    
    self.blurView.frame = CGRectMake(0, 0, 320, 65);
    self.blurView.hidden = NO;
    [self.navigationController setNavigationBarHidden: YES animated:YES];
    
    float start = self.table.frame.origin.y;
    NSLog(@"TabkeH1: %f",start);
    
    [UIView animateWithDuration:0.5 delay:0.0 options:0
                     animations:^{
                         
//                         self.navigationController.navigationBar.frame = CGRectMake(0, 0, 320, 0);
                         
                         self.filterView.frame = CGRectMake(0, screenH, 320, self.filterView.frame.size.height);
                         self.blurView.frame = CGRectMake(0, 0, 320,screenH);
                         [self.blurView refresh];
                         self.table.frame = CGRectMake(0, start, self.table.frame.size.width, self.table.frame.size.height);
                         
                         
                     }
                     completion:^(BOOL finished)
     {
         
     }];
}

-(void)hideFilter
{
    
//    [self.navigationController.navigationBar setOpaque:YES ];
//    self.navigationController.navigationBarHidden = NO;
    [self.navigationController setNavigationBarHidden: NO animated:YES];
    [UIView animateWithDuration:0.5 delay:0.0 options:0
                     animations:^{
                         
//                         self.navigationController.navigationBar.frame = CGRectMake(0, 0, 320, 0);
                         
                         self.filterView.frame = CGRectMake(0, 64, 320, self.filterView.frame.size.height);
                         self.blurView.frame = CGRectMake(0, 0, 320,0);

                     }
                     completion:^(BOOL finished)
     {
         
         [self.blurView removeFromSuperview];
         NSLog(@"TabkeH1: %f",self.table.frame.origin.y);
         
         
     }];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == self.products.count - 1)
    {
        [self addMoreItems];
    }
}


-(void) reloadViewWithCategory:(Categories*) categroy
{
    
    filter.currentFilter = nil;
    if(self.currentCategory != categroy)
    {
        self.contentUnAvailable.image = nil;
        self.contentUnAvailable.backgroundColor = [UIColor whiteColor];
        loading.hidden = NO;
        self.contentUnAvailable.hidden = NO;
        [activityView startAnimating];
    }
    else
    {
        [activityView stopAnimating];
        self.contentUnAvailable.hidden = YES;
    }
    
    self.currentCategory = categroy;
    
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        [self reloadProducts];
    }];

}

-(void) reloadViewWithAds:(KiwiWeeklyAds*) ad
{
    [activityView startAnimating];
    [self.contentUnAvailable addSubview:activityView];
    
    prodcutController.delegate = self;
    
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        [prodcutController getProductsThumbnailForWeeklyAd:ad];
    }];
}

-(void) addMoreItems
{
    int start = (int)self.products.count ;
    int count = NumberOfProductsInEachPage;
    
     if(![self.currentCategory.cat_id isEqualToString:@"2"] && self.currentCategory)
    [prodcutController getProductsThumbnailForCateogryAndItsSubCats:self.currentCategory from:start to:count withFilter:filter.currentFilter];
    
}

-(void) checkNumberOfProducts
{
    self.view.backgroundColor = [UIColor whiteColor];
    if (self.products.count < 1)
    {
        self.table.hidden = YES;
        
        int labelH = 30;
        int width = 50;
        
        noData = [[UILabel alloc] initWithFrame:CGRectMake((self.view.frame.size.width / 2) - (width/2), (self.view.frame.size.height / 2) - (labelH/2), width, labelH)];
        noData.textAlignment = NSTextAlignmentCenter;
        
        noData.text = @"No products under this category";
        noData.font = [UIFont systemFontOfSize:12.0];
        self.contentUnAvailable.hidden = NO;
    }
    else
    {
        self.table.hidden = NO;
        [noData removeFromSuperview];
    }
}

-(void)RequestedProducts:(NSArray*)products isWeeklyAds:(BOOL)weeklyAds
{
    if( products.count > 0)
    {
        
        
        if (!(self.products ) || weeklyAds)
        {
            NSOperationQueue *myQueue = [NSOperationQueue mainQueue];
            [myQueue addOperationWithBlock:^{
                
                self.products = [NSMutableArray arrayWithArray:products];

                [table reloadData];
                [activityView stopAnimating];
                self.contentUnAvailable.hidden = YES;
//                [self checkNumberOfProducts];
            }];
            
            
            
        }
        else
        {
                NSOperationQueue *myQueue = [NSOperationQueue mainQueue];
                [myQueue addOperationWithBlock:^{
                    
                    [activityView stopAnimating];
                    self.contentUnAvailable.hidden = YES;
                    
                    int start = (int)self.products.count ;
                    
                    for (KiwiProductThumbnail * kiwiproduct in products)
                    {
                    if(!self.products)
                        break;
                    
                        
                    [self.products addObject:kiwiproduct];
                    [self.table insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:start -1 inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
                    }
                }];
                
            
        }
    }
    else if(self.products == nil)
    {
        NSOperationQueue *myQueue = [NSOperationQueue mainQueue];
        [myQueue addOperationWithBlock:^{
            
            [self.contentUnAvailable setImage:[UIImage imageNamed:@"bkg-content-unavailable.png"]];
            self.contentUnAvailable.hidden = NO;
            loading.hidden = YES;
            [self.table reloadData];
            NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [formatter setGroupingSeparator:@","];
            [formatter setGroupingSize:3];
            
            self.numberOfitems.text =@"0";
                self.dataInformation.text = [NSString stringWithFormat:@"in %@",self.currentCategory.name];
                [self.numberOfitems sizeToFit];
                
                float start = self.numberOfitems.frame.origin.x + self.numberOfitems.frame.size.width + 5;
                
                self.dataInformation.frame = CGRectMake(start , self.dataInformation.frame.origin.y,250 - start, self.dataInformation.frame.size.height);
        }];
       
        
    }
}

-(void)reloadProducts
{
    self.products = nil;
    if(![self.currentCategory.cat_id isEqualToString:@"2"] && self.currentCategory)
    [prodcutController getProductsThumbnailForCateogryAndItsSubCats:self.currentCategory from:0 to:NumberOfProductsInEachPage withFilter:filter.currentFilter];
    
    
    if(self.currentCategory != nil)
    {
        int numberOfProducts = [prodcutController numberOFProductsInCategoryAndChildern:self.currentCategory];
       HeightestPrice =  [prodcutController HighestPriceinCategory:self.currentCategory];

        NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [formatter setGroupingSeparator:@","];
        [formatter setGroupingSize:3];
        
        NSOperationQueue *myQueue = [NSOperationQueue mainQueue];
        [myQueue addOperationWithBlock:^{
            
           
            
            self.numberOfitems.text =[formatter stringFromNumber:[NSNumber numberWithInt:numberOfProducts]];
            self.dataInformation.text = [NSString stringWithFormat:@"in %@",self.currentCategory.name];
            [self.numberOfitems sizeToFit];
            
            float start = self.numberOfitems.frame.origin.x + self.numberOfitems.frame.size.width + 5;
            
            self.dataInformation.frame = CGRectMake(start , self.dataInformation.frame.origin.y,250 - start, self.dataInformation.frame.size.height);
            
        }];
        
    }
    

//    self.products = [NSMutableArray arrayWithArray:[prodcutController getProductsThumbnailForCateogry:self.currentCategory from:0 to:40]];
   
}


-(void)reloadProductsWithFilter :(NSDictionary*)filter
{

    self.products = nil;
    
     if(![self.currentCategory.cat_id isEqualToString:@"2"] && self.currentCategory)
    [prodcutController getProductsThumbnailForCateogryAndItsSubCats:self.currentCategory from:0 to:NumberOfProductsInEachPage withFilter:filter];
    
    
    if(self.currentCategory != nil)
    {
        int numberOfProducts = [prodcutController numberOFProductsInCategoryAndChildern:self.currentCategory];
        
        HeightestPrice =  [prodcutController HighestPriceinCategory:self.currentCategory];
        
        NSNumberFormatter * formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [formatter setGroupingSeparator:@","];
        [formatter setGroupingSize:3];
        
        NSOperationQueue *myQueue = [NSOperationQueue mainQueue];
        [myQueue addOperationWithBlock:^{

            self.numberOfitems.text =[formatter stringFromNumber:[NSNumber numberWithInt:numberOfProducts]];
            self.dataInformation.text = [NSString stringWithFormat:@"in %@",self.currentCategory.name];
            [self.numberOfitems sizeToFit];
            
            float start = self.numberOfitems.frame.origin.x + self.numberOfitems.frame.size.width + 5;
            
            self.dataInformation.frame = CGRectMake(start , self.dataInformation.frame.origin.y,250 - start, self.dataInformation.frame.size.height);
            
        }];
        
    }
}


-(void)failToLoadProducts
{
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.products.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%i",(int)indexPath.row];
    ProductCell *cell = [table dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:nil options:nil] objectAtIndex:0];
        [cell setRestorationIdentifier: CellIdentifier];
    }
    
    //set cell Data
    
    KiwiProductThumbnail * product = [self.products objectAtIndex:indexPath.row];
    
    cell.title.text = product.name;
    [cell.title sizeToFit];
    
    cell.price.frame = CGRectMake(cell.price.frame.origin.x , cell.title.frame.origin.y + cell.title.frame.size.height + 4, cell.price.frame.size.width, cell.price.frame.size.height);
    
    if(product.price.length > 0)
    cell.price.text = [NSString stringWithFormat:@"AED %0.2f",[product.price floatValue]];
//    product.tag = [[KiwiProductTag alloc] init];
//    product.tag.type =[Utitlities getTag];
    
    
  
    
   cell.deals.text =  product.tag.type ;
    product.tag = [[KiwiProductTag alloc] init];
    product.tag.type = cell.deals.text;
    [cell.deals sizeToFit];
    cell.deals.frame = CGRectMake(cell.deals.frame.origin.x, cell.deals.frame.origin.y, cell.deals.frame.size.width + 10, cell.deals.frame.size.height);
    
    [cell.image setImageWithURL:[Utitlities imageUrl:product.image.imageUrl] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE]];
    
    cell.addToCart.tag = indexPath.row;
    [cell.addToCart addTarget:self action:@selector(addToCart:) forControlEvents:UIControlEventTouchUpInside];
 
    NSString *available = [self ProductAvailablity:product];
    
        if(available.length > 0)
        {
            cell.availability.frame = CGRectMake(cell.availability.frame.origin.x , cell.price.frame.origin.y + cell.price.frame.size.height + 4, cell.availability.frame.size.width, cell.discounts.frame.size.height);
            
        }
    cell.availability.text = available;

    
    
    
    cell.discounts.frame = CGRectMake(cell.discounts.frame.origin.x , cell.availability.frame.origin.y + cell.availability.frame.size.height + 4, cell.discounts.frame.size.width, cell.discounts.frame.size.height);
    cell.discounts.text = product.product_id;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(NSString * )ProductAvailablity:(KiwiProductThumbnail *) product
{
    NSString * available = @"";
    BOOL store,Online;
    
    
    for (KiwiAttribute * attribute in product.attribute)
    {
        if(store && Online)
            break;

        if([attribute.type isEqualToString:@"online availability"] &&
           [attribute.value intValue] > 0
           )
        {
            if([available isEqualToString:@""])
                available = [NSString stringWithFormat:@"Available online"];
            else
                available = [NSString stringWithFormat:@"%@ and available online",available];
            Online = YES;
        }
        
        else if([attribute.type isEqualToString:@"in store availability"] &&
           [attribute.value intValue] > 0
           )
        {
            if([available isEqualToString:@""])
                available = [NSString stringWithFormat:@"Available in store"];
            else
                available = [NSString stringWithFormat:@"%@ and available in store",available];
            
            store = YES;
        }
        
    }

    
    if(store || Online)
        return available;
    
    else
        return @"";
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 122;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProductDetailsViewController * prodetails = [self.storyboard instantiateViewControllerWithIdentifier:@"proDetails"];
    KiwiProductThumbnail * selectedProduct = [self.products objectAtIndex:indexPath.row];
    
    prodetails.product = [prodcutController getProductDetails:selectedProduct.product_id];
    prodetails.product.price = selectedProduct.price;
    prodetails.product.tag = selectedProduct.tag;
    if(self.products.count > NumberOfRecommneded)
    prodetails.recomendedProducts = [NSMutableArray arrayWithArray:[self.products subarrayWithRange:NSMakeRange(self.products.count - NumberOfRecommneded -1, NumberOfRecommneded)]];
    else
        prodetails.recomendedProducts = self.products;
    
    
    [[KiwiNavigationBarController getCenterViewNavigationController] pushViewController:prodetails animated:YES];
    
    
    
}

-(IBAction)addToCart:(id)sender
{
    UIButton * button = (UIButton*)sender;
    
    int cellIndex = (int)button.tag;
    
    KiwiProductThumbnail *kiwiProduct = [self.products objectAtIndex:cellIndex];
    

    
    [self startAnimation:[NSIndexPath indexPathForRow:cellIndex inSection:0] andDestination:CGPointMake(288,35 )];
    
    
    
    [[CartController getsharedInstance] addProduct:kiwiProduct];
}

-(void)startAnimation:(NSIndexPath*)indexPath andDestination:(CGPoint)point;
{
    ProductCell * cell =(ProductCell *) [self.table cellForRowAtIndexPath:indexPath];
    
    UIImageView * image = cell.image;
    
    UIImageView *copyImage = [[UIImageView alloc] initWithImage:image.image];
    
    
    
    CGRect rectInTableView = [table rectForRowAtIndexPath:indexPath];
    CGRect rectInSuperview = [table convertRect:rectInTableView toView:[table superview]];
    
    copyImage.frame = CGRectMake(image.frame.origin.x, rectInSuperview.origin.y + 5
                                 , image.frame.size.width, image.frame.size.height);
    
    [[KiwiNavigationBarController getCenterViewNavigationController].view addSubview:copyImage];
    
    [UIView animateWithDuration:1.0 delay:0.0 options:0
                     animations:^{
                         
                         copyImage.frame = CGRectMake(point.x, point.y
                                                      ,10, 10);
                         
                     }
                     completion:^(BOOL finished)
     {
         [copyImage removeFromSuperview];
     }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
