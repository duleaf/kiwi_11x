//
//  ProductCell.h
//  Kiwi
//
//  Created by Mohammed Salah on ٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView * image;
@property (nonatomic,strong) IBOutlet UILabel * title;
@property (nonatomic,strong) IBOutlet UILabel * price;
@property (nonatomic,strong) IBOutlet UILabel * notes;
@property (nonatomic,strong) IBOutlet UIButton * addToCart;
@property (nonatomic,strong) IBOutlet UILabel * deals;
@property (nonatomic,strong) IBOutlet UILabel * availability;
@property (nonatomic,strong) IBOutlet UILabel * discounts;

@end
