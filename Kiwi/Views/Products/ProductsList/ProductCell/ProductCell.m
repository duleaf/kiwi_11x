//
//  ProductCell.m
//  Kiwi
//
//  Created by Mohammed Salah on ٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "ProductCell.h"

@implementation ProductCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

// hide labels that has no value
-(void)updateLabelsVisiblity
{
    for (UIView * subView in self.contentView.subviews)
    {
        if ([subView isKindOfClass:UILabel.class])
        {
            UILabel * label = (UILabel*) subView;
            
            if (label.text.length == 0)
            {
                label.hidden = YES;
            }
        }
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
