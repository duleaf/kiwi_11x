//
//  ProductListViewController.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Categories.h"
#import "ProductsController.h"
#import "KiwiWeeklyAds.h"
#import "UIERealTimeBlurView.h"


@interface ProductListViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,ProductsDelegate>

@property (nonatomic,strong) IBOutlet UITableView * table;
@property (nonatomic,strong) IBOutlet Categories * currentCategory;
@property (nonatomic,strong) IBOutlet UIView * filterView;
@property (nonatomic,strong) IBOutlet UIImageView * contentUnAvailable;
@property (nonatomic,strong) IBOutlet UILabel * dataInformation;
@property (nonatomic,strong) IBOutlet UILabel * numberOfitems;
@property (nonatomic,strong)  NSMutableArray * products;
@property (nonatomic,strong)  UIERealTimeBlurView *blurView;

-(void) reloadViewWithCategory:(Categories*) categroy;
-(void) reloadViewWithAds:(KiwiWeeklyAds*) ad;
-(void)reloadProductsWithFilter :(NSDictionary*)filter;
-(void)hideFilter;

@end
