//
//  ProductSize.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٠‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "ProductSize.h"
#import <QuartzCore/QuartzCore.h>

@implementation ProductSize

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    _sizes = [[NSMutableArray alloc] init];
    
    for (int i = 1; i < 7; i++)
    {
        [_sizes addObject:[self viewWithTag:i]];
        
        UIButton * button = (UIButton*) [self viewWithTag:i];
        
        [button addTarget:self action:@selector(sizeSelected:) forControlEvents:UIControlEventTouchUpInside];
        button.layer.cornerRadius = 2;
        button.clipsToBounds = YES;
        
        [button.layer setBorderColor: [[UIColor colorWithRed:0.0/255.0f green:122.0/255.0f blue:255.0/255.0f alpha:1.0f] CGColor]];
        [button.layer setBorderWidth: 1.0];
        
    }
}

//-(void) updateSizeForColor:(ProductColorsAndSizes*)color
//{
//    for (int i = 1; i < 7; i++)
//    {
//        [_sizes addObject:[self viewWithTag:i]];
//        
//        UIButton * button = (UIButton*) [self viewWithTag:i];
//        
//        [button addTarget:self action:@selector(sizeSelected:) forControlEvents:UIControlEventTouchUpInside];
//        
//       
//        
//        if([color.colorAvailableSizes containsObject:[NSNumber numberWithInt:i]])
//        {
//            button.backgroundColor = [UIColor colorWithRed:173.0/255.0f green:213.0/255.0f blue:255.0/255.0f alpha:1.0f];
//            button.userInteractionEnabled = YES;
//        }
//        else
//        {
//            button.backgroundColor = [UIColor grayColor];
//            button.userInteractionEnabled = NO;
//        }
//        
//        
//    }
//}



-(IBAction)sizeSelected:(id)sender
{
    UIButton * button = (UIButton*) sender;
    [self.delegate sizeClicked:button.tag];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
