//
//  AttributtSelectorTableViewController.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٣‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterDelegate <NSObject>

-(void) selectedValue:(NSString*)vlaue forKey:(NSString*)type;

@end

@interface AttributtSelectorTableViewController : UITableViewController

@property (nonatomic,strong) NSArray * values;
@property (nonatomic,strong) NSString * type;
@property (nonatomic,strong) id <FilterDelegate> delegate;


@end
