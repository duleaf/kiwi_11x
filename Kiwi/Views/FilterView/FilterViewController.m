//
//  FilterViewController.m
//  Kiwi
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "FilterViewController.h"
#import "ProductListViewController.h"
#import "AttributtSelectorTableViewController.h"
@interface FilterViewController ()
{
    FilterController * filter;
    NSArray * items;
    NSString *Price;
    NSArray * colors;
    AttributtSelectorTableViewController * attributSelector;
    
}
@end

@implementation FilterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)selectedColorIndex:(int)index
{
    [self selectedValue:[self.colorsView.colors objectAtIndex:index] forKey:@"Color"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.currentFilter = [[NSMutableDictionary alloc] init];
    UIBarButtonItem * reset = [[UIBarButtonItem alloc] initWithTitle:@"reset" style:UIBarButtonItemStylePlain target:self action:@selector(resetFilter:)];
    
    self.navigationItem.leftBarButtonItem = reset;
    

    UIBarButtonItem * apply = [[UIBarButtonItem alloc] initWithTitle:@"apply" style:UIBarButtonItemStylePlain target:self action:@selector(applyFilter:)];
    
    self.navigationItem.rightBarButtonItem = apply;
    
    
    self.labelSlider = [[NMRangeSlider alloc] initWithFrame:CGRectMake(20, 20, 280, 50)];
    [self.labelSlider addTarget:self action:@selector(labelSliderChanged:) forControlEvents:UIControlEventValueChanged];
    
    self.lowerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 15)];
    self.lowerLabel.text = @"0";
    
    self.upperLabel = [[UILabel alloc] initWithFrame:CGRectMake(250, 0, 20, 50)];
    self.upperLabel.text = @"100";
    
    self.upperLabel.font = [UIFont systemFontOfSize:12];
    self.lowerLabel.font = [UIFont systemFontOfSize:12];
    [self.lowerLabel sizeToFit];
    [self.upperLabel sizeToFit];
    
    
    
    [self configureLabelSlider];
    
    self.title = self.filterTitle;
    self.table.backgroundColor  = [UIColor clearColor];
    self.table.backgroundView = nil;
    [self.table setOpaque:YES];
    self.view.backgroundColor = [UIColor clearColor];
    filter = [[FilterController alloc] init];
    items = [filter getAllAvailableAttributeswithCategory:self.category.cat_id];
    self.colorsView = [[[NSBundle mainBundle] loadNibNamed:@"ColorsView" owner:nil options:nil] firstObject];
    self.colorsView.backgroundColor  = [UIColor clearColor];
    self.colorsView.colors = [filter getAllAvailableVlauesForAttrubite:@"Color" withCategory:self.category.cat_id];
    [self.colorsView addColorButtons];
    self.colorsView.delegate = self;
	// Do any additional setup after loading the view.
}


- (void) configureLabelSlider
{
    if (self.HightesPrice < 1)
    {
        self.HightesPrice = 10;
    }
    
    self.labelSlider.minimumValue = 0;
    self.labelSlider.maximumValue = self.HightesPrice;
    
    self.labelSlider.lowerValue = 0;
    self.labelSlider.upperValue = self.HightesPrice;
    
    self.labelSlider.minimumRange = self.HightesPrice/10;
    
 
    
    [self updateSliderLabels];
}

- (void) updateSliderLabels
{
    // You get get the center point of the slider handles and use this to arrange other subviews
    
    CGPoint lowerCenter;
    lowerCenter.x = (self.labelSlider.lowerCenter.x + self.labelSlider.frame.origin.x);
    lowerCenter.y = (self.labelSlider.center.y - 30.0f);
    self.lowerLabel.center = lowerCenter;
    int lower = (int)self.labelSlider.lowerValue;
    self.lowerLabel.text = [NSString stringWithFormat:@"%i", lower];
    
    CGPoint upperCenter;
    upperCenter.x = (self.labelSlider.upperCenter.x + self.labelSlider.frame.origin.x);
    upperCenter.y = (self.labelSlider.center.y - 30.0f);
    self.upperLabel.center = upperCenter;
    int upper = (int)self.labelSlider.upperValue;
    self.upperLabel.text = [NSString stringWithFormat:@"%i", upper];
    NSLog(@"UpperV: %i",upper);
    [self.lowerLabel sizeToFit];
    [self.upperLabel sizeToFit];
    Price = [NSString stringWithFormat:@"%i,%i",lower,upper];
    
}

// Handle control value changed events just like a normal slider
- (IBAction)labelSliderChanged:(NMRangeSlider*)sender
{
    [self updateSliderLabels];
}

-(void)removeUneededFilters
{
    NSArray * unNeededTypies = [NSArray arrayWithObjects:@"Code",@"Color", nil];
    
    int itemIndex = [items indexOfObject:@"Code"];
}

-(IBAction)resetFilter:(id)sender
{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"filer.plist"]; //3
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path]) //4
    {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"filter" ofType:@"plist"]; //5
        
        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
    }
    
    
    self.currentFilter = [[NSMutableDictionary alloc] init];
    
    [self.currentFilter writeToFile:path atomically:YES];
    [self dismissView:nil];
}

-(IBAction)applyFilter:(id)sender
{
    [self selectedValue:Price forKey:@"price"];
    [self dismissView:nil];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//-(void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    self.table.frame = CGRectMake(self.table.frame.origin.x,
//                              self.view.frame.size.height - 50 - scrollView.contentOffset.y,
//                              self.table.frame.size.width,
//                              self.table.frame.size.height + scrollView.contentOffset.y);
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return items.count ;
}

-(IBAction)dismissView:(id)sender
{
    UINavigationController * nav =  [KiwiNavigationBarController getCenterViewNavigationController];
    
    ProductListViewController * product = [nav.viewControllers firstObject];
    [product hideFilter];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%i",(int)indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if(indexPath.row == 0)
    {
        [cell addSubview:self.labelSlider];
        [cell addSubview:self.lowerLabel];
        [cell addSubview:self.upperLabel];
        cell.backgroundColor = [UIColor clearColor];

    }
    
    else if(indexPath.row == 1)
    {
        cell.backgroundColor = [UIColor clearColor];
        [cell addSubview:self.colorsView];
    }
    
    
    else
    {
        cell.textLabel.text = [items objectAtIndex:indexPath.row ];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        return 70;

    }
    if(indexPath.row == 1)
    {
        return [self.colorsView getHeight];
    }
    
    else
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < 2)
    {
        return;
    }
    
    attributSelector = [[AttributtSelectorTableViewController alloc] initWithStyle:UITableViewStylePlain];
    
    attributSelector.values = [filter getAllAvailableVlauesForAttrubite:[items objectAtIndex:indexPath.row] withCategory:self.category.cat_id];
    attributSelector.type = [items objectAtIndex:indexPath.row];
    attributSelector.delegate = self;
    [self.navigationController pushViewController:attributSelector animated:YES];
}

-(void) selectedValue:(NSString*)vlaue forKey:(NSString*)type
{

    [self.currentFilter setObject:vlaue forKey:type];
    UINavigationController * nav =  [KiwiNavigationBarController getCenterViewNavigationController];
    
    ProductListViewController * product = [nav.viewControllers firstObject];
    
    [product reloadProductsWithFilter:self.currentFilter];
    
    
}

@end
