//
//  ColorsView.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٥‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ColorSelectionDelegate <NSObject>

-(void)selectedColorIndex:(int)index;

@end


@interface ColorsView : UIView

@property(nonatomic, weak) NSMutableArray * colors;

@property(nonatomic,strong) id <ColorSelectionDelegate> delegate;

-(int)getHeight;
-(void)addColorButtons;

@end
