//
//  ColorsView.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٥‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "ColorsView.h"
#import "ColorCell.h"
@implementation ColorsView

static int height = 30;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)addColorButtons
{

    int numberOfCells = 4;
    int Xspace = 16;
    int Yspace = 16;
    
    int numberOfRows = (int)self.colors.count /numberOfCells;
    
    if(self.colors.count % numberOfCells> 0)
        numberOfRows ++;
    
    for (int row = 0; row < numberOfRows ; row++)
    {
        for (int column = 0;  column < numberOfCells; column++)
        {
            ColorCell *newColor = [[[NSBundle mainBundle] loadNibNamed:@"ColorCell" owner:nil options:nil] firstObject];
            
            
            newColor.frame = CGRectMake(column * (Xspace + 50) + Xspace, row * (Yspace + 70) + 30, 50, 70);
            [newColor roundedImage];
            
//            [newColor addTarget:self action:@selector(colorSelected:) forControlEvents:UIControlEventTouchUpInside];
            
            
            int currentIndex = (row * 4) + column;
            
            if(currentIndex >= self.colors.count)
                break;
            
            newColor.tag = currentIndex;
            
//            UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(colorSelected:)];
//            [newColor addGestureRecognizer:singleTapGestureRecognizer];
            
            NSString * color = [self.colors objectAtIndex:currentIndex];
            [self addSubview:newColor];

            newColor.color = color;
            
            if(![color isEqualToString:@"N/A"] &&( [color rangeOfString:@","].location != NSNotFound))
            {
                newColor.colorTitle.text = [[color componentsSeparatedByString:@","] firstObject];
                NSString * colorString =[[color componentsSeparatedByString:@","] objectAtIndex:1];
                newColor.colorImage.backgroundColor = [Utitlities ColorFromHexString:colorString];
            }
            
            else
            {
                newColor.colorTitle.text = color;
                newColor.colorImage.backgroundColor = [Utitlities getRandomColor];
            }
            

        }
    }
    
    height = 50 + ((Yspace + 70)  * numberOfRows);
    self.frame = CGRectMake(0, 0, 320, height);
    
}

-(int)getHeight
{
    return height;
}

-(IBAction)colorSelected:(id)sender
{
    UIView * view = (UIView*) sender;
    [self.delegate selectedColorIndex:(int)view.tag];
}
// 4
/*- (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 return [[UICollectionReusableView alloc] init];
 }*/

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
