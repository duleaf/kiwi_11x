//
//  ColorCell.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٥‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "Color.h"
@interface ColorCell : UIView


@property(nonatomic,strong) IBOutlet UIImageView * colorImage;
@property(nonatomic,strong) IBOutlet UILabel * colorTitle;
@property(nonatomic,strong) NSString * color;




- (id)initWithFrame:(CGRect)frame;
-(id)init;
-(void)roundedImage;

@end
