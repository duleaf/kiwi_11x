
//
//  ColorCell.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٥‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "ColorCell.h"

@implementation ColorCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        _colorImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 50)];
        _colorTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 55, 60, 20)];
        _colorTitle.font = [UIFont fontWithName:@"Arial" size:14.0];
        self.frame = CGRectMake(frame.origin.y, frame.origin.y, 60, 70);
        [self roundedImage];
    }
    return self;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        _colorImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        _colorTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 55, 50, 20)];
        _colorTitle.font = [UIFont fontWithName:@"Arial" size:14.0];
        self.frame = CGRectMake(0, 0, 50, 50);
        [self roundedImage];
    }
    return self;
}

-(void)roundedImage
{
    [self.colorImage.layer setCornerRadius:self.colorImage.frame.size.height/2];
    self.colorImage.clipsToBounds = YES;
    
    // border
    [self.colorImage.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.colorImage.layer setBorderWidth:1.5f];
    
    // drop shadow
    [self.colorImage.layer setShadowColor:[UIColor whiteColor].CGColor];
    [self.colorImage.layer setShadowOpacity:0.8];
    [self.colorImage.layer setShadowRadius:3.0];
    [self.colorImage.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
