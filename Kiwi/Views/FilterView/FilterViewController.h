//
//  FilterViewController.h
//  Kiwi
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorsView.h"
#import "FilterController.h"
#import "Categories.h"
#import "NMRangeSlider.h"



@interface FilterViewController : UIViewController<ColorSelectionDelegate>

@property (strong, nonatomic)  NMRangeSlider *labelSlider;
@property (nonatomic,strong) ColorsView * colorsView;
@property (nonatomic,strong) IBOutlet UITableView * table;
@property (nonatomic,strong) NSArray *products;
@property (nonatomic,strong) NSString *filterTitle;
@property (nonatomic,strong) UIImageView * bluredImage;
@property (nonatomic,strong) NSMutableDictionary * currentFilter;
@property (nonatomic,strong) Categories * category;
@property (nonatomic,strong) UILabel *lowerLabel;
@property (nonatomic,strong) UILabel *upperLabel;
@property (nonatomic)    float HightesPrice;




-(IBAction)dismissView:(id)sender;

@end
