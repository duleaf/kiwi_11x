//
//  WeeklyAdsViewController.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٦‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "WeeklyAdsViewController.h"
#import "KiwiWeeklyAds.h"
#import "WeeklyAdsController.h"
@interface WeeklyAdsViewController ()
{
    AdsWheel * wheel;
    WeeklyAdsController * controller;
    ProductsController * prodcutController;
    NSMutableArray * allProducts;
}
@end

@implementation WeeklyAdsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    controller = [[WeeklyAdsController alloc] init];

    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        self.productList = [self.storyboard instantiateViewControllerWithIdentifier:@"Categories"];

    }];
    
    wheel = [[[NSBundle mainBundle] loadNibNamed:@"AdsWheel" owner:nil options:nil] firstObject];
    
    wheel.weeklyAds = [NSMutableArray arrayWithArray:[self getAds]];

    wheel.delegate = self;
    [wheel loadAdsWheelWithFrame:CGRectMake(0, 105, 320, self.view.frame.size.height )];
    
    self.productList.filterView.hidden = YES;
    self.productList.view.frame = CGRectMake(0, self.view.frame.size.height, 320, 50);
    self.productList.table.frame = self.productList.view.frame;
    
    allProducts = [WeeklyAdsController getWeeklyAdsProducts];
    prodcutController = [[ProductsController alloc] init];
    [NSArray arrayWithArray:]
    [self.view addSubview:wheel.scrollView];
    [self.view addSubview:wheel.lowerView];
    [self.view addSubview:self.productList.table];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)AddSelectedWithIndex:(int)adIndex andBigMode:(BOOL)big
{

    
    [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionLayoutSubviews animations:^
    {
        
        if(big)
        {
           
            self.productList.table.frame = CGRectMake(0,280, 320, self.view.frame.size.height -230);
        }
        else
        {
            
            self.productList.table.frame = CGRectMake(0, self.view.frame.size.height, 320, 50);

        }
    } completion:^(BOOL finished)
     {
         if(allProducts.count > adIndex)
         [self.productList RequestedProducts:[allProducts objectAtIndex:adIndex] isWeeklyAds:YES];
     }];
}

-(NSArray*)getAds
{

    
    NSArray * ads = [WeeklyAdsController getWeeklyAds];
    
    
    if(ads.count <1)
        return [NSArray new];
    
    KiwiWeeklyAds * ad = [ads objectAtIndex:0];
    ad.adImage = [[KiwiAdImage alloc] init];
    ad.adImage.imageUrl = @"http://akimages.shoplocal.com/dyn_rppi/650.0.85.0/Target/large/140914_p01mw_04ays.jpg";
    

    ad = [ads objectAtIndex:1];
    ad.adImage = [[KiwiAdImage alloc] init];
    ad.adImage.imageUrl = @"http://akimages.shoplocal.com/dyn_rppi/621.0.85.0/Target/large/140914_p03mw_een0o.jpg";
    
//    
    ad = [ads objectAtIndex:2];
    ad.adImage = [[KiwiAdImage alloc] init];
    ad.adImage.imageUrl = @"http://akimages.shoplocal.com/dyn_rppi/621.0.85.0/Target/large/140914_p05mw_dlsjs.jpg";
//    [ads addObject:ad];
//    
    ad = [ads objectAtIndex:3];
    ad.adImage = [[KiwiAdImage alloc] init];
    ad.adImage.imageUrl = @"http://akimages.shoplocal.com/dyn_rppi/621.0.85.0/Target/large/140914_p02mw_ux5ce.jpg";
//    [ads addObject:ad];
//    
//    
    ad = [ads objectAtIndex:4];
    ad.adImage = [[KiwiAdImage alloc] init];
    ad.adImage.imageUrl = @"http://akimages.shoplocal.com/dyn_rppi/621.0.85.0/Target/large/140914_pbcommw_taqa2.jpg";
//    [ads addObject:ad];
//    
//    
    ad = [ads objectAtIndex:5];
    ad.adImage = [[KiwiAdImage alloc] init];
    ad.adImage.imageUrl = @"http://akimages.shoplocal.com/dyn_rppi/621.0.85.0/Target/large/140914_pacommw_besim.jpg";
//    [ads addObject:ad];
//    
    ad = [ads objectAtIndex:6];
    ad.adImage = [[KiwiAdImage alloc] init];
    ad.adImage.imageUrl = @"http://akimages.shoplocal.com/dyn_rppi/621.0.85.0/Target/large/140914_p06mw_08dd0.jpg";
//    [ads addObject:ad];
//    
    ad = [ads objectAtIndex:7];
    ad.adImage = [[KiwiAdImage alloc] init];
    ad.adImage.imageUrl = @"http://akimages.shoplocal.com/dyn_rppi/621.0.85.0/Target/large/140914_p07mw_s9bol.jpg";
//    [ads addObject:ad];
//    
    ad = [ads objectAtIndex:8];
    ad.adImage = [[KiwiAdImage alloc] init];
    ad.adImage.imageUrl = @"http://akimages.shoplocal.com/dyn_rppi/621.0.85.0/Target/large/140914_p09mw_kpm3z.jpg";
//    [ads addObject:ad];
//    
//    
//    
    return ads;
}

-(void)loadAllAds
{
    for (KiwiWeeklyAds * ad in wheel.weeklyAds)
    {
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            prodcutController.delegate = self;
            [prodcutController getProductsThumbnailForWeeklyAd:ad];
        }];
    }
}

-(void)RequestedProducts:(NSArray*)products isWeeklyAds:(BOOL)weeklyAds
{
    [allProducts addObject:products];
    
    NSOperationQueue *myQueue = [NSOperationQueue mainQueue];
    [myQueue addOperationWithBlock:^{
        
        [self.productList.table reloadData];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
