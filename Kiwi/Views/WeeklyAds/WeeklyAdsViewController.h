//
//  WeeklyAdsViewController.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٦‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListViewController.h"
#import "AdsWheel.h"

@interface WeeklyAdsViewController : UIViewController <AdsDelegate,ProductsDelegate>

@property (nonatomic,strong) ProductListViewController * productList;


@end
