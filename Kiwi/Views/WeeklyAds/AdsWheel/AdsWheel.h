//
//  AdsWheel.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٦‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AdsDelegate <NSObject>

-(void)AddSelectedWithIndex:(int)adIndex andBigMode:(BOOL)big;

@end


@interface AdsWheel : UIView<UIScrollViewDelegate>


@property (nonatomic,weak) id <AdsDelegate> delegate;
@property (nonatomic,strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) IBOutlet UILabel *pageNumber;
@property (nonatomic,strong) IBOutlet UILabel *numberOfItems;
@property (nonatomic,strong) IBOutlet UIView  *lowerView;
@property (nonatomic,strong) NSMutableArray * weeklyAds;
@property (nonatomic,strong) NSMutableArray * subViews;



-(void)loadAdsWheelWithFrame:(CGRect)frame;

@end
