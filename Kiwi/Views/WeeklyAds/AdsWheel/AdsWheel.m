//
//  AdsWheel.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٦‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "AdsWheel.h"
#import "KiwiWeeklyAds.h"

#define ANIMATION_TIME 1.0f

#define BIG_SPACE 25
#define BIG_BUTTON_HEIGHT 320
#define BIG_BUTTON_WIDTH 270

#define SMALL_SPACE 10
#define SMALL_BUTTON_HEIGHT 120
#define SMALL_BUTTON_WIDTH 100

#define PAGE_NUMBER_VIEW_HEIGHT 30




@interface AdsWheel ()
{
    bool bigViewMode;
    CGRect originalFrame;
    CGRect buttonsFrame;
    KiwiWeeklyAds * selectedAdd;
    UIButton * centerAd;
}
@end

@implementation AdsWheel



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)loadAdsWheelWithFrame:(CGRect)frame
{
    //set pages text
    _numberOfItems.text = [NSString stringWithFormat:@"(%i items)",(int) self.weeklyAds.count];
    _pageNumber.text = [NSString stringWithFormat:@"Page 1"];

    
    originalFrame = frame;
    buttonsFrame = CGRectMake(0, 0,BIG_BUTTON_WIDTH, originalFrame.size.height - 25);
    self.scrollView.frame = CGRectMake(0, frame.origin.y + 5, 320, originalFrame.size.height - PAGE_NUMBER_VIEW_HEIGHT);;
    self.lowerView.frame = CGRectMake(0, originalFrame.size.height - PAGE_NUMBER_VIEW_HEIGHT, 320, PAGE_NUMBER_VIEW_HEIGHT);
    
    bigViewMode = true;
    self.scrollView.tag = 1;
    self.scrollView.autoresizesSubviews = NO;
    self.scrollView.delegate = self;
    self.subViews = [[NSMutableArray alloc] init];
    
    
    //spaces betwen items
    
    
    for (int i = 0; i < self.weeklyAds.count; i++)
    {
        UIButton * ad = [[UIButton alloc] initWithFrame:CGRectMake((i * (BIG_BUTTON_WIDTH + (2 * BIG_SPACE)) + BIG_SPACE), 10, BIG_BUTTON_WIDTH, BIG_BUTTON_HEIGHT)];
        
        //Adding shadow to each button
        ad.autoresizingMask = UIViewAutoresizingNone;
        ad.layer.shadowColor = [UIColor darkGrayColor].CGColor;
        ad.layer.shadowOpacity = 0.8;
        ad.layer.shadowRadius = 2;
        ad.layer.shadowOffset = CGSizeMake(-1.0f, 2.0f);
        
        ad.tag = i;
        ad.backgroundColor = [UIColor whiteColor];
        KiwiWeeklyAds * ads = [self.weeklyAds objectAtIndex:i];
        
        UIImageView * img = [[UIImageView alloc] init];
        
        //
        [ad setImage:[UIImage imageNamed:DEFAULT_IMAGE] forState:UIControlStateNormal];
        
        [img setImageWithURL:[NSURL URLWithString:ads.adImage.imageUrl] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType)
         {
             [ad setImage:img.image forState:UIControlStateNormal];
         }];
        
        [ad addTarget:self action:@selector(AddSelected:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.scrollView addSubview:ad];
        [self.subViews addObject:ad];
    }

    //each button has width 270 and right and left spaces 25+25 so each page (button+spaces) wodth is 320
    self.scrollView.contentSize = CGSizeMake(320 * self.weeklyAds.count, self.scrollView.contentSize.height);
}

-(void)convertWheelAdsAnimatatedlyWithSelectedIndex:(int)selectedIndex
{
    bigViewMode = !bigViewMode;

    if(!bigViewMode)
    {
        
       
        
        
        [UIView animateWithDuration:ANIMATION_TIME delay:0.0f options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionLayoutSubviews animations:^{
            
            
            [self.scrollView setContentOffset:CGPointMake((selectedIndex  * 110), self.scrollView.contentOffset.y) animated:NO];
            
            int offset = 110;
            int i = 0;
            
            for (UIView * v in self.scrollView.subviews)
            {
                if([v isKindOfClass:UIButton.class])
                {
                    
                    v.frame = CGRectMake(offset, 10, SMALL_BUTTON_WIDTH, SMALL_BUTTON_HEIGHT);
                    offset +=  110;
                    i++;
                }
            }
            
            self.lowerView.frame = CGRectMake(0, 250, 320, 30);
 
         } completion:^(BOOL finished)
         {
              self.scrollView.contentSize = CGSizeMake((SMALL_BUTTON_WIDTH + SMALL_SPACE) * (self.weeklyAds.count + 2) , self.scrollView.contentSize.height);
         }];
        
    }
    
    else
    {
        self.scrollView.contentSize = CGSizeMake(320 * self.weeklyAds.count, self.scrollView.contentSize.height);

        
        [UIView animateWithDuration:ANIMATION_TIME delay:0.0f options:UIViewAnimationOptionCurveLinear|UIViewAnimationOptionLayoutSubviews animations:^{
            
           [self.scrollView setContentOffset:CGPointMake(selectedIndex * 320, self.scrollView.contentOffset.y) animated:NO];
        
            self.lowerView.frame = CGRectMake(0, originalFrame.size.height - 30, 320, 30);
            int i = 0;
            
            for (UIView * v in self.scrollView.subviews)
            {
                if([v isKindOfClass:UIButton.class])
                {
                    v.frame = CGRectMake(i * (BIG_BUTTON_WIDTH + (2 * BIG_SPACE)) + BIG_SPACE, 5, BIG_BUTTON_WIDTH, BIG_BUTTON_HEIGHT);
                    i++;
                }
            }
            
        } completion:nil];
        
    }
    
    
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if(!decelerate)
    [self adjustScrollItemsOffset];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self adjustScrollItemsOffset];
}

-(void)adjustScrollItemsOffset
{
    int pageWidth = 320;
    
    if(!bigViewMode)
    {
        pageWidth = 110;
    }
    
    
    int xOffset = self.scrollView.contentOffset.x;
    int newOffset = (xOffset /pageWidth) * pageWidth;
    int rem = xOffset % pageWidth;
    
    if(rem > (pageWidth)/3)
        newOffset = ((xOffset /pageWidth)+1) * pageWidth;
    
    if(newOffset >= pageWidth * self.weeklyAds.count)
    
        newOffset -= pageWidth;
        
    [self.scrollView setContentOffset:CGPointMake(newOffset , self.scrollView.contentOffset.y) animated:YES] ;
    
 
    int pageIndex = newOffset / pageWidth ;
    
    if (pageIndex >= self.weeklyAds.count) {
        pageIndex = self.weeklyAds.count -1;
    }
    
    if (!bigViewMode) {
        [centerAd.layer setBorderWidth:0];
    }
    
    centerAd = [self.subViews objectAtIndex:pageIndex];
    selectedAdd = [self.weeklyAds objectAtIndex:pageIndex];
    
    if (!bigViewMode) {
        [centerAd.layer setBorderColor:[UIColor blueColor].CGColor];
        [centerAd.layer setBorderWidth:1.5f];
        [self.delegate AddSelectedWithIndex:pageIndex andBigMode:!bigViewMode];
    }
    
    

    
    _pageNumber.text = [NSString stringWithFormat:@"Page %i",pageIndex + 1];
}

-(void)setCenterItemAtIndex:(int) selectedIndex
{
    int pageWidth = 320;
    
    if(!bigViewMode)
    {
        pageWidth = 110;
    }
    
    
    int xOffset = (selectedIndex )* pageWidth ;
    int newOffset = (xOffset /pageWidth) * pageWidth;
    int rem = xOffset % pageWidth;
    
    if(rem > (pageWidth)/3)
        newOffset = ((xOffset /pageWidth)+1) * pageWidth;
    
    [self.scrollView setContentOffset:CGPointMake(newOffset , self.scrollView.contentOffset.y) animated:YES] ;
    
 
    
}

-(IBAction)AddSelected:(id)sender
{
    UIButton * button = (UIButton*)sender;
    int adIndex = (int)button.tag;
    
   
        [centerAd.layer setBorderWidth:0];
    
    
    if(button == centerAd || bigViewMode)
    {
        [self convertWheelAdsAnimatatedlyWithSelectedIndex:adIndex];
    }
    
    else
    {
        [self setCenterItemAtIndex: adIndex];
        
    }
    
    [self.delegate AddSelectedWithIndex:adIndex andBigMode:!bigViewMode];
    centerAd = button;
    selectedAdd = [self.weeklyAds objectAtIndex:adIndex];
    
    if (!bigViewMode) {
        [centerAd.layer setBorderColor:[UIColor blueColor].CGColor];
        [centerAd.layer setBorderWidth:1.5f];
    }
    
    _pageNumber.text = [NSString stringWithFormat:@"Page %i",adIndex + 1];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
