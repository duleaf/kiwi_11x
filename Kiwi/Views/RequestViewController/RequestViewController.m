//
//  RequestViewController.m
//  Farook
//
//  Created by Marwan Ayman on 5/17/14.
//  Copyright (c) 2014 Mohammed Salah. All rights reserved.
//

#import "RequestViewController.h"

@interface RequestViewController ()

@end

@implementation RequestViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.title = @"Request";
//    self.navigationItem.hidesBackButton = YES;

}

-(IBAction)login:(id)sender
{
    if ([self.userName.text isEqualToString:@"Kiwi"] && [self.password.text isEqualToString:@"Kiwi"])
    {
        ;//login
    }
}
-(IBAction)facebookLogin:(id)sender
{
    
}
-(IBAction)registeration:(id)sender
{
    
}
-(IBAction)getBackYourPassword:(id)sender
{
    
}


-(IBAction)cancelRequest:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated{

//    else if([defaults integerForKey:@"arabic"]==1){
//        self.title = @"نموذج طلب المنتج";
//        self.nametitle.text = @"الاسم بالكامل";
//        self.companytitle.text = @"اسم الشركة";
//        self.nmbertitle.text = @"رقم الهاتف";
//        self.emailtitle.text = @"البريد الالكتروني";
//        [self.submitBtn setTitle:@"ارسل" forState:UIControlStateNormal];
//        
//    }
//    else if([defaults integerForKey:@"arabic"]==0){
//        
//        self.title = @"Request Form";
//        self.nametitle.text = @"Full name";
//        self.companytitle.text = @"Company Name";
//        self.nmbertitle.text = @"Contact Number";
//        self.emailtitle.text = @"Contact Email";
//        [self.submitBtn setTitle:@"Submit" forState:UIControlStateNormal];
//        
//    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)showAlertViewWithTitle:(NSString*)title andMessage:(NSString*)message{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message delegate:self cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil, nil];
    [alert show];//[NSString stringWithFormat:@"%@",@""];
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (BOOL) validPhone:(NSString*) phoneString {
    
    for (int i=0; i<phoneString.length; i++) {
        int asciiCode = [phoneString characterAtIndex:i];
        if(asciiCode<48||asciiCode>57)
        {
            NSLog(@"ttttt %c",[phoneString characterAtIndex:i]);
            return false;
            
        }
        
    }
    return true;
}
- (IBAction)submit:(id)sender {
    
//    if([UserPrefrences getArabiclanguage] == 1)
//    {
//        if (
//            self.fullName.text.length==0) {
//            [self showAlertViewWithTitle:@"تنبيه" andMessage:@"الرجاء ادخال اسمك"];
//        }
//        else if (self.companyName.text.length==0) {
//            [self showAlertViewWithTitle:@"تنبيه" andMessage:@"الرجاء ادخال اسم شركتك"];
//        }
//        else if (self.contactNumber.text.length==0) {
//            [self showAlertViewWithTitle:@"تنبيه" andMessage:@"الرجاء ادخال رقم هاتفك"];
//        }
//        else if (![self validPhone:self.contactNumber.text]) {
//            [self showAlertViewWithTitle:@"تنبيه" andMessage:@"الرجاء ادخال رقم هاتف صحيح"];
//        }
//        else if (self.contactEmail.text.length==0) {
//            [self showAlertViewWithTitle:@"تنبيه" andMessage:@"الرجاء ادخال عنوان بريدك الالكتروني"];
//        }
//        else if (![self NSStringIsValidEmail:self.contactEmail.text]) {
//            [self showAlertViewWithTitle:@"تنبيه" andMessage:@"الرجاء ادخال عنوان بريد الكتروني بشكل صحيح"];
//        }
//        else{
//            UserData * ob = [[UserData alloc]init];
//            ob.name = self.fullName.text;
//            ob.company = self.companyName.text;
//            ob.email = self.contactEmail.text;
//            [HUD show:YES];
//            [controller submitUserData:ob];
//            
//            
//        }
//    }
//    
//    else
//    {
//        if (
//            self.fullName.text.length==0) {
//            [self showAlertViewWithTitle:@"Alert" andMessage:@"Please enter your full name"];
//        }
//        else if (self.companyName.text.length==0) {
//            [self showAlertViewWithTitle:@"Alert" andMessage:@"Please enter your company name"];
//        }
//        else if (self.contactNumber.text.length==0) {
//            [self showAlertViewWithTitle:@"Alert" andMessage:@"Please enter your contact number"];
//        }
//        else if (![self validPhone:self.contactNumber.text]) {
//            [self showAlertViewWithTitle:@"Alert" andMessage:@"Please enter a vaild contact number"];
//        }
//        else if (self.contactEmail.text.length==0) {
//            [self showAlertViewWithTitle:@"Alert" andMessage:@"Please enter your email address"];
//        }
//        else if (![self NSStringIsValidEmail:self.contactEmail.text]) {
//            [self showAlertViewWithTitle:@"Alert" andMessage:@"Please enter a vaild email address"];
//        }
//        else{
//            UserData * ob = [[UserData alloc]init];
//            ob.name = self.fullName.text;
//            ob.company = self.companyName.text;
//            ob.email = self.contactEmail.text;
//            [HUD show:YES];
//            [controller submitUserData:ob];
//            
//            
//        }
//    }
//    

    
}
-(void)processServerAllLocCompleted:(BOOL)status{
//    [HUD hide:NO];
//    if (status) {
//        [self showAlertViewWithTitle:@"Success" andMessage:@"Order sent successfully!"];
//    }
//    else
//         [self showAlertViewWithTitle:@"Faild" andMessage:@"Faild to submit order!"];
}
@end
