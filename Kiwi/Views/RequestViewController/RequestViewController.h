//
//  RequestViewController.h
//  Farook
//
//  Created by Marwan Ayman on 5/17/14.
//  Copyright (c) 2014 Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestViewController : UIViewController<UITextFieldDelegate>


//@property (weak, nonatomic) IBOutlet UITextField *fullName;
//@property (weak, nonatomic) IBOutlet UITextField *companyName;
//@property (weak, nonatomic) IBOutlet UITextField *contactNumber;
//@property (weak, nonatomic) IBOutlet UITextField *contactEmail;
//- (IBAction)submit:(id)sender;
//@property (weak, nonatomic) IBOutlet UILabel *nametitle;
//@property (weak, nonatomic) IBOutlet UILabel *companytitle;
//@property (weak, nonatomic) IBOutlet UILabel *nmbertitle;
//@property (weak, nonatomic) IBOutlet UILabel *emailtitle;
//@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@property (weak, nonatomic) IBOutlet UIButton *FBLogin;
@property (weak, nonatomic) IBOutlet UIButton *Login;
@property (weak, nonatomic) IBOutlet UIButton *signUP;
@property (weak, nonatomic) IBOutlet UIButton *frogotPassword;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *password;



-(IBAction)login:(id)sender;
-(IBAction)facebookLogin:(id)sender;
-(IBAction)registeration:(id)sender;
-(IBAction)getBackYourPassword:(id)sender;

@end
