//
//  CheckOutTableViewCell.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٤‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KiwiProduct.h"

@interface CheckOutTableViewCell : UITableViewCell

@property (nonatomic,strong) UIView  * deleteView;
@property (nonatomic,strong) UIView  * counterView;
@property (nonatomic,strong) UIView  * midView;
@property (nonatomic,strong) UIButton  * deleteButton;
@property (nonatomic,strong) UIImageView  * productImage ;
@property (nonatomic,strong) UILabel  * title;
@property (nonatomic,strong) UILabel  * desc;
@property (nonatomic,strong) UILabel  * price;
@property (nonatomic,strong) UITextField  * counter;
@property (nonatomic,strong) KiwiProduct  * product;

-(void)loadViews;
-(void)setData:(KiwiProduct*)product;
-(IBAction)checkOut:(id)sender;
@end
