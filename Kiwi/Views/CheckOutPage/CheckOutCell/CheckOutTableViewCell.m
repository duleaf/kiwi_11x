//
//  CheckOutTableViewCell.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٤‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "CheckOutTableViewCell.h"
#define CellWidth 320

@implementation CheckOutTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

-(void)loadViews
{
    self.deleteView = [[[NSBundle mainBundle] loadNibNamed:@"CheckOutTableViewCell" owner:nil options:nil] objectAtIndex:1];
    self.deleteView.frame = CGRectMake(-50, 0, 50, 120);

    
    self.midView = [[[NSBundle mainBundle] loadNibNamed:@"CheckOutTableViewCell" owner:nil options:nil] objectAtIndex:2];
    self.midView.frame = CGRectMake(0, 0, CellWidth, 120);

    self.counterView = [[[NSBundle mainBundle] loadNibNamed:@"CheckOutTableViewCell" owner:nil options:nil] objectAtIndex:3];
    self.counterView.frame = CGRectMake(320, 0, 70, 120);
    

    
    
    self.deleteButton = (UIButton*) [self.deleteView viewWithTag:1];
    self.productImage = (UIImageView*)[self.midView viewWithTag:1];
    self.title =  (UILabel*)[self.midView viewWithTag:2];
    self.desc = (UILabel*)[self.midView viewWithTag:3];
    self.price = (UILabel*)[self.midView viewWithTag:4];
    self.counter = (UITextField*)[self.counterView viewWithTag:1];
    
}



-(void)setData:(KiwiProduct*)product
{
    self.product = product;
    
    [self.productImage setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE]];
    
    self.title.text = self.product.name;
    self.counter.text = [NSString stringWithFormat:@"%i",product.productNumberOfPecies.intValue];
    product.price = [NSString stringWithFormat:@"AED %.02f",[Utitlities getRundomNumber:200 andMin:10 isFloat:YES]];
    self.price.text = self.product.price;
    
    [self.price sizeToFit];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
