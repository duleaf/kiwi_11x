//
//  CheckOutPageViewController.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٤‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "CheckOutPageViewController.h"
#import "CheckOutTableViewCell.h"
#import "KiwiProduct.h"
#import "RequestViewController.h"
#import "KiwiUser.h"
#import "UserDefaultes.h"
#define TotalViewHeight 50
#define NavBarHeight 65


@interface CheckOutPageViewController ()
{
    float totalPrice;
}
@end

@implementation CheckOutPageViewController

@synthesize table;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.totalView.autoresizingMask = UIViewAutoresizingNone;
    self.totalView.frame = CGRectMake(0, self.view.frame.size.height - TotalViewHeight, 320, TotalViewHeight);
    
    self.table.frame = CGRectMake(self.table.frame.origin.x, NavBarHeight, self.table.frame.size.width, self.view.frame.size.height - TotalViewHeight - NavBarHeight);
    
    self.leftButon.target = self;
    self.rightButon.target = self;
    self.leftButon.title = @"Edit";
    
    [self.leftButon setAction:@selector(edit:)];
    [self.rightButon setAction:@selector(checkOut:)];
    
    
    self.leftButon.possibleTitles = [NSSet setWithObjects:@"Edit", @"Done", nil];
    [self calculateTotal];
    
    [self.total sizeToFit];
}

-(void)viewWillAppear:(BOOL)animated
{
    
//    if([self.sidePanelController centerPanelHidden])
//    {
//        [self.sidePanelController showCenterPanelAnimated:YES];
//        [self.sidePanelController showRightPanelAnimated:YES];
//        
//    }
}
-(IBAction)checkOut:(id)sender
{
    KiwiUser * user = [UserDefaultes getUser];
    
    if([CartController getsharedInstance].products.count > 0 && user.cardNumber.length < 1)
    {
//        [self.sidePanelController setCenterPanelHidden:YES];
        RequestViewController * request = [self.storyboard instantiateViewControllerWithIdentifier:@"request"];
        
        [self.navigationController pushViewController:request animated:YES];
    }
    else
    {
        
        [[[UIAlertView alloc] initWithTitle:@"Cart" message:@"Do you want to continue?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm", nil] show];
    }
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        self.total.text = @"AED 0.00";
        [CartController getsharedInstance].products = [[NSMutableArray alloc] init];
        [KiwiNavigationBarController updateCartBadge:0];
        [self.table reloadData];
        [KiwiNavigationBarController hidePresnetedView];
    }
}

-(void) calculateTotal
{
    totalPrice = 0;
    
    for (int i = 0 ; i < [CartController getsharedInstance].products.count; i++)
    {
        KiwiProduct* product = [[CartController getsharedInstance].products objectAtIndex:i];
        
        if(product.price.length > 0)
        {
            NSString * price =  [product.price substringFromIndex:3];

            float itemPrice = product.productNumberOfPecies.intValue * [price floatValue];
            totalPrice+= itemPrice;
            
        }
    }
    
    self.total.text = [NSString stringWithFormat:@"AED %0.2f",totalPrice];
    [self.total sizeToFit];
}

-(IBAction)edit:(id)sender
{
    static Boolean * editValue = false;
    
    if (editValue) {
        editValue = false;
    }
    else
    {
        editValue = true;
    }
    
    [self editMode:editValue];
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"Count: %i",(int)[CartController getsharedInstance].products.count);
    
    int height = 120 * (int)[CartController getsharedInstance].products.count;
    
    if(height > self.view.frame.size.height - TotalViewHeight - NavBarHeight)
        height =  self.view.frame.size.height - TotalViewHeight - NavBarHeight;
    
    self.table.frame = CGRectMake(self.table.frame.origin.x, self.table.frame.origin.y, self.table.frame.size.width, height);
    
    return [CartController getsharedInstance].products.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%i",(int)indexPath.row];
    CheckOutTableViewCell *cell = [table dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CheckOutTableViewCell" owner:nil options:nil] firstObject];
     
        [cell loadViews];
        [cell addSubview:cell.midView];
        
        
        
        [cell.deleteButton addTarget:self action:@selector(showDeleteButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell setData:[[CartController getsharedInstance].products objectAtIndex:indexPath.row]];
    }
    
    cell.counter.delegate = self;
    cell.counter.tag = indexPath.row;
    
    cell.deleteButton.tag = indexPath.row;
    [cell.deleteButton addTarget:self action:@selector(DeleteCell:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row ==  [CartController getsharedInstance].products.count - 1)
        [self calculateTotal];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    KiwiProduct * product = [[CartController getsharedInstance].products objectAtIndex:textField.tag];
    
    product.productNumberOfPecies = [NSNumber numberWithInt:[textField.text intValue]];
    
    [self calculateTotal];
    [textField resignFirstResponder];
    
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[CartController getsharedInstance] removeProduct:(int)indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
        
        [self calculateTotal];
    }
}

-(IBAction)DeleteCell:(id)sender
{
    int index = (int)((UIButton*)sender).tag;
    
    [[CartController getsharedInstance] removeProduct:index];
    
    [self.table deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
    
    [self calculateTotal];
}


- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)editMode:(BOOL)edit
{
    [self.table setEditing:edit animated:YES];

    
    if (edit)
    {

        self.leftButon.style = UIBarButtonSystemItemDone;
        [self.leftButon setTitle:@"Done"];
        for (int i = 0 ; i < 5; i++)
        {
            CheckOutTableViewCell *cell  = (CheckOutTableViewCell *) [self.table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            
//            [cell.midView addSubview:cell.deleteView];
            [cell addSubview:cell.counterView];
            [cell addSubview:cell.deleteView];
            
            
            
            cell.counter.text = [NSString stringWithFormat:@"%i",cell.product.productNumberOfPecies.intValue];
            
            [UIView animateWithDuration:0.5 delay:0.0 options:0
                             animations:^{
                                 
                                 
                                 cell.counterView.frame = CGRectMake(250, 0, 70, 120);
//                                 cell.deleteView.frame = CGRectMake(0, 0, 50, 120);
                                 cell.midView.frame = CGRectMake(50, 0, 270, 120);
//
                             }
                             completion:nil];
        }
    }
    
    else
    {
        self.leftButon.style = UIBarButtonSystemItemEdit;
        [self.leftButon setTitle:@"Edit"] ;
        for (int i = 0 ; i < 5; i++)
        {
            CheckOutTableViewCell *cell  = (CheckOutTableViewCell *) [self.table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            cell.price.hidden = NO;
            
            [UIView animateWithDuration:0.5 delay:0.0 options:0
                             animations:^{
                                 
                                 
                                 cell.counterView.frame = CGRectMake(320, 0, 70, 120);
//                                 cell.deleteView.frame = CGRectMake(-50, 0, 50, 120);
                                 cell.midView.frame =  CGRectMake(0, 0, 320, 120);
                                 
                             }
                             completion:^(BOOL finished)
             {
                 [cell.counterView removeFromSuperview];
                 [cell.deleteView removeFromSuperview];
             }];
        }
        
    }
    
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}


-(IBAction)showDeleteButton:(id)sender
{
    CheckOutTableViewCell *cell = ( CheckOutTableViewCell *)[table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.backgroundColor = [UIColor greenColor];
            view.frame = CGRectMake(-82, 0, 320, 120);
            
            
            
            NSLog(@"ViewSups %@",[view subviews]);
        }
    }
    
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
