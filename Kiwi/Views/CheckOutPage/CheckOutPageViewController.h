//
//  CheckOutPageViewController.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٤‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CartController.h"


@interface CheckOutPageViewController : UIViewController <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>


@property (nonatomic,strong) IBOutlet UITableView * table;
@property (nonatomic,strong) IBOutlet  UIBarButtonItem * leftButon;
@property (nonatomic,strong) IBOutlet  UIBarButtonItem * rightButon;
@property (nonatomic,strong) IBOutlet  UILabel * total;
@property (nonatomic,strong) IBOutlet UIView  * totalView;



-(void) calculateTotal;
-(IBAction)checkOut:(id)sender;

@end
