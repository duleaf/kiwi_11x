//
//  HomeView.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٥‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeView : UIViewController


@property (nonatomic,strong) IBOutlet UIButton * cart;

@end
