//
//  HomeView.m
//  Kiwi
//
//  Created by Mohammed Salah on ٢٥‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "HomeView.h"
#import "KiwiOrder.h"
#import "AppDelegate.h"
#import "CartController.h"
#import "UIBarButtonItem+Badge.h"
@interface HomeView ()

@end

@implementation HomeView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
    //2
    
    KiwiOrder * order = [NSEntityDescription insertNewObjectForEntityForName:@"Order" inManagedObjectContext:appDelegate.managedObjectContext];
    
    order.order_id = @"1";
    order.user_id = @"1";
    
    NSError * error;
    [appDelegate.managedObjectContext save:&error];

    UIImage *image = [UIImage imageNamed:@"nav-header-icon-cart_ios7.png"];
    self.cart = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cart.frame = CGRectMake(0,0,30, 30);
    [self.cart setBackgroundImage:image forState:UIControlStateNormal];
    
    // Make BarButton Item
    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:self.cart];
    self.navigationItem.rightBarButtonItem = navLeftButton;
    self.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%i",(int)[CartController getsharedInstance].products.count];
    self.navigationItem.rightBarButtonItem.badge.hidden = YES;
 
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav-header-icon-search_ios7.png"] style:UIBarButtonItemStylePlain target:self action:@selector(Search:)];
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:navLeftButton , searchButton, nil]];
    
    
    
    [KiwiNavigationBarController setAddToCart:self.navigationItem.rightBarButtonItem andButton:self.cart];

    
//    _sidebarButton.tintColor = [UIColor colorWithWhite:0.96f alpha:0.2f];
//
//    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
//    _sidebarButton.target = self.revealViewController;
//    _sidebarButton.action = @selector(revealToggle:);
    
    // Set the gesture


    [self setNeedsStatusBarAppearanceUpdate];
}

-(IBAction)Search:(id)sender
{
    
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
