//
//  PaymentChooserViewController.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٢‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KiwiUser.h"
@interface PaymentChooserViewController : UITableViewController

@property (nonatomic,strong) KiwiUser * user;

@end
