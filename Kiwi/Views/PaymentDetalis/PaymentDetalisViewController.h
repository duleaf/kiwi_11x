//
//  PaymentDetalisViewController.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٢‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KiwiUser.h"
@interface PaymentDetalisViewController : UITableViewController

@property (nonatomic,strong) KiwiUser * user;
@property (nonatomic,strong) IBOutlet UITextField * name;
@property (nonatomic,strong) IBOutlet UITextField * cardNumber;
@property (nonatomic,strong) IBOutlet UITextField * exDate;
@property (nonatomic,strong) IBOutlet UITextField * CVV;
@property (nonatomic,strong) IBOutlet UITextField * billingAddress;

-(IBAction)submitRequest:(id)sender;
@end
