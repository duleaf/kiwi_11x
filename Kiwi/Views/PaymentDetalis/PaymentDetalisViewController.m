//
//  PaymentDetalisViewController.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٢‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "PaymentDetalisViewController.h"
#import "UserDefaultes.h"
#import "CheckOutPageViewController.h"
@interface PaymentDetalisViewController ()

@end

@implementation PaymentDetalisViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.user = [[KiwiUser alloc] init];
    [self.user fillDummyData];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];    // Do any additional setup after loading the view.
}

-(IBAction)submitRequest:(id)sender
{
    if([self.name.text isEqualToString:@""] || [self.cardNumber.text isEqualToString:@""]
       || [self.exDate.text isEqualToString:@""] || [self.CVV.text isEqualToString:@""]
       || [self.billingAddress.text isEqualToString:@""] )
    {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Please fill all data" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else
     {
         self.user.cardNumber = self.cardNumber.text;
         self.user.CVV = self.CVV.text;
         self.user.exDate = self.exDate.text;
         
         [UserDefaultes saveUser:self.user];
         CheckOutPageViewController * checkout = [self.navigationController.viewControllers firstObject];
         
         [checkout checkOut:nil];
         [self.navigationController popToRootViewControllerAnimated:YES];
         
         
     }
         
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
