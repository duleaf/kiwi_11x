//
//  Categories.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٨‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KiwiCategories : NSObject

@property (nonatomic, retain) NSString * cat_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * categoryIconUrl;
@property (nonatomic, retain) NSString * parent_id;

@end
