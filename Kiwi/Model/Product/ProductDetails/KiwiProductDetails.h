//
//  ProductDetails.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "KiwiProduct.h"
#import "KiwiProductImage.h"
@interface KiwiProductDetails : KiwiProduct


@property (nonatomic,strong) KiwiProductImage * image;
@property (nonatomic,strong) NSArray * Stores;


@end
