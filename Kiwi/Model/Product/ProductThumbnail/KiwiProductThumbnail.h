//
//  ProductThumbnail.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KiwiProduct.h"
#import "KiwiProductImage.h"
@interface KiwiProductThumbnail : KiwiProduct

@property (nonatomic,strong) KiwiProductImage * image;


@end
