//
//  Product.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KiwiCategories.h"
#import "KiwiProductTag.h"
#import "KiwiAttribute.h"

@interface KiwiProduct : NSObject

@property (nonatomic,strong) NSString *product_id;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) KiwiCategories *category;
@property (nonatomic,strong) KiwiProductTag *tag;
@property (nonatomic,strong) NSArray *attribute;
@property (nonatomic, retain) NSString * weekly_id;
@property (nonatomic,strong) NSNumber * productNumberOfPecies;



@end
