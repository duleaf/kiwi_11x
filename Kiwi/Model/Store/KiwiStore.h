//
//  Store.h
//  Kiwi
//
//  Created by Mohammed Salah on ٣٠‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KiwiAttribute.h"
#import "Store.h"

@interface KiwiStore : NSObject


@property(nonatomic,strong) NSString * store_id;
@property(nonatomic,strong) NSString * name;
@property(nonatomic,strong) NSString * storeAddress;
@property(nonatomic,strong) NSString * lang;
@property(nonatomic,strong) NSString * lat;
@property(nonatomic,strong) KiwiAttribute * attribute;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;


-(id)initWithStore:(Store*)store;

@end
