//
//  Store.m
//  Kiwi
//
//  Created by Mohammed Salah on ٣٠‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "KiwiStore.h"
#import "AppDelegate.h"
@implementation KiwiStore


-(id)init
{
    self= [super init];
    
    if(self)
    {
        AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
        //2

        NSPersistentStoreCoordinator *coordinator = [appDelegate persistentStoreCoordinator];
        if (coordinator != nil) {
            self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            [self.managedObjectContext setPersistentStoreCoordinator: coordinator];
        }
    }
    
    return self;
    
}
-(id)initWithStore:(Store*)store
{
    self = [super init];
    
    if (self)
    {
        self.name = store.name;
        self.store_id = store.sotre_id;
        self.lang = store.lang;
        self.lat = store.lat;
        
    }
    return self;
}

-(KiwiStore*) getKiwiStoreFromStore: (Store*) store
{
    KiwiStore * kiwiStore = [[KiwiStore alloc] init];
    kiwiStore.name = store.name;
    kiwiStore.store_id = store.sotre_id;
    kiwiStore.lang = store.lang;
    kiwiStore.lat = store.lat;
    
    
    return kiwiStore;
}

-(KiwiStore*)getStoreFromId:(NSString*)store_id
{
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Store"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"sotre_id==%@",store_id]; // If required to fetch specific vehicle
    fetchRequest.predicate=predicate;
    Store * store=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
    
    return [self getKiwiStoreFromStore:store];
}

@end
