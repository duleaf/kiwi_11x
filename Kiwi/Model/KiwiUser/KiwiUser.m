//
//  KiwiUser.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٢‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "KiwiUser.h"

@implementation KiwiUser


-(void)fillDummyData
{
    
    self.fName = @"Mohammed";
    self.lName = @"Salah";
    self.companyName = @"Kiwi";
    self.streetAddress = @"7  140 st";
    self.suit = @"Unkonw";
    self.town = @"Maadi";
    self.state = @"Cairo";
    self.zipCode = @"0222";
    self.country = @"Egypt";
    self.phone = @"009123456789";
    self.email = @"user@email.com";

}


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.fName forKey:@"fName"];
    [encoder encodeObject:self.lName forKey:@"lName"];
    [encoder encodeObject:self.companyName forKey:@"companyName"];
    [encoder encodeObject:self.streetAddress forKey:@"streetAddress"];
    [encoder encodeObject:self.suit forKey:@"suit"];
    [encoder encodeObject:self.town forKey:@"town"];
    [encoder encodeObject:self.state forKey:@"state"];
    [encoder encodeObject:self.zipCode forKey:@"zipCode"];
    [encoder encodeObject:self.country forKey:@"country"];
    [encoder encodeObject:self.phone forKey:@"phone"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.exDate forKey:@"exDate"];
    [encoder encodeObject:self.CVV forKey:@"CVV"];
    [encoder encodeObject:self.cardNumber forKey:@"cardNumber"];
    
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.fName = [decoder decodeObjectForKey:@"fName"];
        self.lName = [decoder decodeObjectForKey:@"lName"];
        self.companyName = [decoder decodeObjectForKey:@"companyName"];
        self.streetAddress = [decoder decodeObjectForKey:@"streetAddress"];
        self.suit = [decoder decodeObjectForKey:@"suit"];
        self.town = [decoder decodeObjectForKey:@"town"];
        self.state = [decoder decodeObjectForKey:@"state"];
        self.zipCode = [decoder decodeObjectForKey:@"zipCode"];
        self.country = [decoder decodeObjectForKey:@"country"];
        self.phone = [decoder decodeObjectForKey:@"phone"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.CVV = [decoder decodeObjectForKey:@"CVV"];
        self.cardNumber = [decoder decodeObjectForKey:@"cardNumber"];
        self.exDate = [decoder decodeObjectForKey:@"exDate"];

        
    }
    return self;
}

@end
