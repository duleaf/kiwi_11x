//
//  KiwiUser.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٢‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KiwiUser : NSObject<NSCoding>

@property (nonatomic,strong) NSString * fName;
@property (nonatomic,strong) NSString * lName;
@property (nonatomic,strong) NSString * companyName;
@property (nonatomic,strong) NSString * streetAddress;
@property (nonatomic,strong) NSString * suit;
@property (nonatomic,strong) NSString * town;
@property (nonatomic,strong) NSString * state;
@property (nonatomic,strong) NSString * zipCode;
@property (nonatomic,strong) NSString * country;
@property (nonatomic,strong) NSString * phone;
@property (nonatomic,strong) NSString * email;
@property (nonatomic,strong) NSString * cardNumber;
@property (nonatomic,strong) NSString * exDate;
@property (nonatomic,strong) NSString * CVV;

-(void)fillDummyData;

@end
