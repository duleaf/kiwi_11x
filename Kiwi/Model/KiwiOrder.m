//
//  Order.m
//  Kiwi
//
//  Created by Mohammed Salah on ٢‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "KiwiOrder.h"
#import "AppDelegate.h"

@implementation KiwiOrder

@dynamic order_id;
@dynamic user_id;
@dynamic product_id;
@dynamic placedTime;


- (id)init
{
    self = [super init];
    
    if (self) {
        //1
        AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
        //2
        self.managedObjectContext = appDelegate.managedObjectContext;
        
    }
    
    return self;
}

-(void) insertOrder
{
    KiwiOrder * order = [NSEntityDescription insertNewObjectForEntityForName:@"KiwiOrder" inManagedObjectContext:self.managedObjectContext];
    
    order.order_id = @"1";
    order.user_id = @"1";
    
    NSError * error;
    [self.managedObjectContext save:&error];
}

@end
