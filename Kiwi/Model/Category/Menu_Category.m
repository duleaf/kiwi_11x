//
//  Category.m
//  Kiwi
//
//  Created by Mohammed Salah on ٢٨‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "Menu_Category.h"


@implementation Menu_Category

@dynamic cat_id;
@dynamic name;
@dynamic icon;
@dynamic parent_id;


@end
