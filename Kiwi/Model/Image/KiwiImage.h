//
//  Image.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KiwiImage : NSObject

@property (nonatomic,strong) NSString *image_id;
@property (nonatomic,strong) NSString *imageUrl;


@end
