//
//  ProductImage.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KiwiImage.h"
#import "ProductImage.h"
@interface KiwiProductImage : KiwiImage

@property (nonatomic,strong) NSString *product_id;
//@property (nonatomic,strong) NSString *name; ORDER
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;


-(KiwiProductImage*)getImageFromId:(NSString*) product_id;

@end
