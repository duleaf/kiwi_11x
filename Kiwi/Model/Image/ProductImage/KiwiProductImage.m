//
//  ProductImage.m
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "KiwiProductImage.h"
#import "AppDelegate.h"

@implementation KiwiProductImage



-(id)init
{
    self= [super init];
    
    if(self)
    {
        AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
        //2

        NSPersistentStoreCoordinator *coordinator = [appDelegate persistentStoreCoordinator];
        if (coordinator != nil) {
            self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            [self.managedObjectContext setPersistentStoreCoordinator: coordinator];
        }
    }
    
    return self;
    
}


-(KiwiProductImage*) getKiwiProductImageFromAttribute: (ProductImage*) image
{
    KiwiProductImage * kiwiImage = [[KiwiProductImage alloc] init];
    
    kiwiImage.product_id = image.product_id;
    kiwiImage.image_id = image.image_id;
    kiwiImage.imageUrl = image.imageUrl;
    
    return kiwiImage;
}

-(KiwiProductImage*)getImageFromId:(NSString*) product_id
{
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"ProductImage"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"product_id==%@",product_id]; // If required to fetch specific vehicle
    fetchRequest.predicate=predicate;
    ProductImage * img=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
    
    return [self getKiwiProductImageFromAttribute:img];
}



@end
