//
//  AdImage.h
//  Kiwi
//
//  Created by Mohammed Salah on ٣٠‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "KiwiImage.h"

@interface KiwiAdImage : KiwiImage

@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * order;//Order
@property (nonatomic,strong) NSString * weeklyad_id;


@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;


-(KiwiAdImage*)getImageFromId:(NSString*) product_id;



@end
