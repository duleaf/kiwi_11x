//
//  Attribute.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Attribute.h"
@interface KiwiAttribute : NSObject

@property (nonatomic,strong) NSString *attribute_id;
@property (nonatomic,strong) NSString *detials;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSString *value;
@property (nonatomic,strong) NSString *colorCode;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

-(id)initWithAttribute:(NSString*)attribute_id;
-(KiwiAttribute*)getStoreFromId:(NSString*) attribute_id;
-(NSArray*)getProductFromId:(NSString*) product_id;
@end
