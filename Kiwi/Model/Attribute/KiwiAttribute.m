//
//  Attribute.m
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "KiwiAttribute.h"
#import "AppDelegate.h"

@implementation KiwiAttribute


-(id)init
{
    self= [super init];
    
    if(self)
    {
        AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
        //2
        
        NSPersistentStoreCoordinator *coordinator = [appDelegate persistentStoreCoordinator];
        if (coordinator != nil) {
            self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            [self.managedObjectContext setPersistentStoreCoordinator: coordinator];
        }
    }
    
    return self;
    
}

-(id)initWithAttribute:(NSString*)attribute_id
{
    self = [self init];
    
    if (self)
    {
        Attribute * attribute = [self getAttributeFromId:attribute_id];
        self.type = attribute.type;
        self.value = attribute.value;
        self.detials = attribute.details;
        self.attribute_id = attribute.attribute_id;
    }
    
    return self;
}

-(KiwiAttribute*) getKiwiAttributeFromAttribute: (Attribute*) attribute
{
    KiwiAttribute * kiwiAttribute = [[KiwiAttribute alloc] init];
    
    kiwiAttribute.type = attribute.type;
    kiwiAttribute.value = attribute.value;
    kiwiAttribute.detials = attribute.details;
    kiwiAttribute.attribute_id = attribute.attribute_id;
    
    return kiwiAttribute;
}

-(NSArray*)getProductFromId:(NSString*) product_id
{
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Attribute"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"product_id==%@ AND (type == %@ OR type == %@ OR type == %@)",product_id,@"in store availability",@"online availability",@"Brand"]; // If required to fetch specific vehicle
    fetchRequest.predicate=predicate;
    NSArray * attris=[self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    NSMutableArray * atts = [[NSMutableArray alloc] init];
    
    for(Attribute * attribute in attris )
    {
        [atts addObject:[self getKiwiAttributeFromAttribute:attribute]];
    }
    
    return [NSArray arrayWithArray:atts];
}

-(KiwiAttribute*)getAttributeFromId:(NSString*) attribute_id
{
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Attribute"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"attribute_id==%@",attribute_id]; // If required to fetch specific vehicle
    fetchRequest.predicate=predicate;
    Attribute * att=[self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    return [self getKiwiAttributeFromAttribute:att];
}


@end
