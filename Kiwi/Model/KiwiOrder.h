//
//  Order.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface KiwiOrder : NSManagedObject

@property (nonatomic, retain) NSString * order_id;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * product_id;
@property (nonatomic, retain) NSDate * placedTime;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;


-(void) insertOrder;

@end
