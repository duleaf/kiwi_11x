//
//  WeeklyAds.h
//  Kiwi
//
//  Created by Mohammed Salah on ٣٠‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KiwiAdImage.h"
@interface KiwiWeeklyAds : NSObject

@property (nonatomic,strong) NSString *weeklyAds_id;
@property (nonatomic,strong) NSArray *stores;
@property (nonatomic,strong) KiwiAdImage *adImage;
@property (nonatomic,strong) NSDate * startDate;
@property (nonatomic,strong) NSDate * endDate;




@end
