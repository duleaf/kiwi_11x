//
//  ProductTag.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductTag.h"
@interface KiwiProductTag : NSObject

@property (nonatomic,strong) NSString *tag_id;
@property (nonatomic,strong) NSString *prodcut_id;
@property (nonatomic,strong) NSString *discount;
@property (nonatomic,strong) NSString *store_id;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSString *endDate;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

-(KiwiProductTag*)getProductFromId:(NSString*) product_id;

@end
