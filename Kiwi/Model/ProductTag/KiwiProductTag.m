//
//  ProductTag.m
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "KiwiProductTag.h"
#import "AppDelegate.h"
@implementation KiwiProductTag


-(id)init
{
    self= [super init];
    
    if(self)
    {
        AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
        //2

        NSPersistentStoreCoordinator *coordinator = [appDelegate persistentStoreCoordinator];
        if (coordinator != nil) {
            self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            [self.managedObjectContext setPersistentStoreCoordinator: coordinator];
        }
    }
    
    return self;
    
}

-(KiwiProductTag*) getKiwiTagFromTag: (ProductTag*) tag
{
    KiwiProductTag * kiwiTag = [[KiwiProductTag alloc] init];
    
    kiwiTag.type = tag.type;
    kiwiTag.store_id = tag.store_id;
    kiwiTag.discount = tag.disscount;
    kiwiTag.prodcut_id = tag.productTag_id;
    kiwiTag.endDate = tag.endDate;
    
    return kiwiTag;
}

-(KiwiProductTag*)getProductFromId:(NSString*) product_id
{
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"ProductTag"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"product_id==%@",product_id]; // If required to fetch specific vehicle
    fetchRequest.predicate=predicate;
    ProductTag * tag=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
    
    return [self getKiwiTagFromTag:tag];
}

@end
