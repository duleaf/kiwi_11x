//
//  ProductImage.h
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Image.h"


@interface ProductImage : Image

@property (nonatomic, retain) NSString * order_id;
@property (nonatomic, retain) NSString * product_id;

@end
