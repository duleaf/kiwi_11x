//
//  Order.h
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Order : NSManagedObject

@property (nonatomic, retain) NSString * order_id;
@property (nonatomic, retain) NSDate * placedTime;
@property (nonatomic, retain) NSString * product_id;
@property (nonatomic, retain) NSString * user_id;

@end
