//
//  WeeklyAds.m
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import "WeeklyAds.h"


@implementation WeeklyAds

@dynamic adImage_id;
@dynamic endDate;
@dynamic name;
@dynamic startDate;
@dynamic store_id;
@dynamic weeklyAd_id;

@end
