//
//  Categories.h
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Categories : NSManagedObject

@property (nonatomic, retain) NSString * cat_id;
@property (nonatomic, retain) NSString * icon;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * parent_id;

@end
