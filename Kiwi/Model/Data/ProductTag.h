//
//  ProductTag.h
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ProductTag : NSManagedObject

@property (nonatomic, retain) NSString * disscount;
@property (nonatomic, retain) NSString * endDate;
@property (nonatomic, retain) NSString * product_id;
@property (nonatomic, retain) NSString * productTag_id;
@property (nonatomic, retain) NSString * store_id;
@property (nonatomic, retain) NSString * type;

@end
