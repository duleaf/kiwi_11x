//
//  Product.m
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import "Product.h"


@implementation Product

@dynamic category;
@dynamic image_id;
@dynamic name;
@dynamic price;
@dynamic product_id;
@dynamic weekly_id;
@dynamic dependancy;
@end
