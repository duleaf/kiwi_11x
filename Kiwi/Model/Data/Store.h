//
//  Store.h
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Store : NSManagedObject

@property (nonatomic, retain) NSString * lang;
@property (nonatomic, retain) NSString * lat;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * sotre_id;

@end
