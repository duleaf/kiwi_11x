//
//  Product.h
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Product : NSManagedObject

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * image_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * price;
@property (nonatomic, retain) NSString * product_id;
@property (nonatomic, retain) NSString * weekly_id;
@property (nonatomic, retain) NSString * dependancy;

@end
