//
//  Attribute.m
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import "Attribute.h"


@implementation Attribute

@dynamic attribute_id;
@dynamic details;
@dynamic product_id;
@dynamic store_id;
@dynamic type;
@dynamic value;
@dynamic category_id;


@end
