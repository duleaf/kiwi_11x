//
//  WeeklyAds.h
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface WeeklyAds : NSManagedObject

@property (nonatomic, retain) NSString * adImage_id;
@property (nonatomic, retain) NSDate * endDate;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSString * store_id;
@property (nonatomic, retain) NSString * weeklyAd_id;

@end
