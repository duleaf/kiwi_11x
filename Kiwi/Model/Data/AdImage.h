//
//  AdImage.h
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Image.h"


@interface AdImage : Image

@property (nonatomic, retain) NSString * order_id;
@property (nonatomic, retain) NSString * title;

@end
