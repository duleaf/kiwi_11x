//
//  Image.h
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Image : NSManagedObject

@property (nonatomic, retain) NSString * image_id;
@property (nonatomic, retain) NSString * imageUrl;
@property (nonatomic, retain) NSString * code;
@end
