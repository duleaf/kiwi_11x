//
//  ProductTag.m
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import "ProductTag.h"


@implementation ProductTag

@dynamic disscount;
@dynamic endDate;
@dynamic product_id;
@dynamic productTag_id;
@dynamic store_id;
@dynamic type;

@end
