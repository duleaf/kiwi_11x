//
//  Attribute.h
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Attribute : NSManagedObject

@property (nonatomic, retain) NSString * attribute_id;
@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSString * product_id;
@property (nonatomic, retain) NSString * store_id;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) NSString * category_id;

@end
