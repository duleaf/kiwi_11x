//
//  Order.m
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import "Order.h"


@implementation Order

@dynamic order_id;
@dynamic placedTime;
@dynamic product_id;
@dynamic user_id;

@end
