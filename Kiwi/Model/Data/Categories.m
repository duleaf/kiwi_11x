//
//  Categories.m
//  TestDHlibxls
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import "Categories.h"


@implementation Categories

@dynamic cat_id;
@dynamic icon;
@dynamic name;
@dynamic parent_id;

@end
