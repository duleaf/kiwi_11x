//
//  UserDefaultes.m
//  Brazilian English Teacher
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import "UserDefaultes.h"

@implementation UserDefaultes


+(void)saveUser:(KiwiUser* )kiwiUser
{
    NSUserDefaults * defaults = [[NSUserDefaults alloc] init];
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:kiwiUser];
    [defaults setObject:encodedObject forKey:@"kiwiUser"];
    [defaults synchronize];
}

+(KiwiUser*) getUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:@"kiwiUser"];
    KiwiUser *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
    
}

@end
