//
//  UserDefaultes.h
//  Brazilian English Teacher
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//
//

#import <Foundation/Foundation.h>
#import "KiwiUser.h"
@interface UserDefaultes : NSObject

+(void)saveUser:(KiwiUser*)kiwiUser;
+(KiwiUser*) getUser;

@end
