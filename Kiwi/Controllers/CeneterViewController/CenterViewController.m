//
//  CeneterViewController.m
//  Kiwi
//
//  Created by Mohammed Salah on ٢٨‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "CenterViewController.h"
#import "ProductListViewController.h"




@implementation CenterViewController



-(void)NewCategorySelected:(Categories*) category andLevel:(int) level
{
    if(level == 0)
    {
        NSString * viewIdentifier = @"Home";
        
        UIViewController * newView = [[KiwiNavigationBarController getSharedInstance].storyBoard instantiateViewControllerWithIdentifier:viewIdentifier];
        
        [KiwiNavigationBarController replaceCenterView:newView];
    }
    
    else
    {
        if ([category.parent_id isEqualToString:@"0"])
        {
            self.CurrentCategory = category;
      
                NSString * viewIdentifier = category.name;
                
                UIViewController * newView = [[KiwiNavigationBarController getSharedInstance].storyBoard instantiateViewControllerWithIdentifier:viewIdentifier];
                
                [KiwiNavigationBarController replaceCenterView:newView];
            
        }
        
        else if ([self.CurrentCategory.cat_id isEqualToString:@"2"])
        {
            UINavigationController * navgation = [KiwiNavigationBarController getCenterViewNavigationController];
            
            ProductListViewController * products = [[navgation viewControllers] firstObject];
            products.products = nil;
            [products reloadViewWithCategory:category];
            
        }
        
//        [self checkLastIndex:category];
    }
    
}




@end
