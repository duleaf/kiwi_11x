//
//  CeneterViewController.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٨‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Categories.h"
#import "LeftMenuController.h"

@interface CenterViewController : NSObject

@property (nonatomic ,strong) Categories * CurrentCategory;

-(void)NewCategorySelected:(Categories*) category andLevel:(int) level;

@end
