//
//  Utitlities.m
//  Kiwi
//
//  Created by Mohammed Salah on ٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "Utitlities.h"

@implementation Utitlities



+(CGRect)getLableSizeFromText:(NSString*)text andLable:(UILabel*)lable
{
    NSString *message = text;
    CGSize maximumLabelSize = CGSizeMake(lable.frame.size.width,1000);
    
    // use font information from the UILabel to calculate the size
    CGSize expectedLabelSize = [message sizeWithFont:lable.font constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByWordWrapping];
    
    // create a frame that is filled with the UILabel frame data
    CGRect newFrame = lable.frame;
    
    // resizing the frame to calculated size
    newFrame.size.height = expectedLabelSize.height + lable.font.xHeight;
    
    // put calculated frame into UILabel frame
    
    return newFrame;
}

+(double) getRundomNumber : (int) max andMin:(int)min isFloat:(BOOL)isfloat
{

    max *= 100;
    
    int randNum = rand() % (max - min) + min; //create the random number.
    

    if (isfloat) {
        return randNum/100;
    }
    else
    return ((int) randNum / 100);
}

+(UIColor*) getRandomColor
{
    int red = [Utitlities getRundomNumber:255 andMin:1 isFloat:YES];
    int green = [Utitlities getRundomNumber:255 andMin:1 isFloat:YES];
    int blue = [Utitlities getRundomNumber:255 andMin:1 isFloat:YES];
    
    NSLog(@"Red:%d Green:%d Blue:%d",red,green,blue);
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}

+(NSURL *)imageUrl:(NSString*) imgUrl
{
    
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://demo.izworks.vn/farook/uploads/%@",imgUrl]];
}

+(NSString*)getTag
{
     NSArray * tags = [NSArray arrayWithObjects:@"",@"Dubai offers",@"sale",@"outlet sale",@"final sale",@"best price",@"", nil];
    
    return  [tags objectAtIndex:[Utitlities getRundomNumber:6 andMin:0 isFloat:NO]];
}


+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+(UIColor *)ColorFromHexString:(NSString *) hexString
{
    float red, green, blue, alpha;
    [Utitlities SKScanHexColor:hexString  :&red :&green :&blue :&alpha];
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+(void) SKScanHexColor:(NSString *) hexString :(float *) red :(float *) green :(float *) blue :(float *) alpha
{
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    if (red) { *red = ((baseValue >> 24) & 0xFF)/255.0f; }
    if (green) { *green = ((baseValue >> 16) & 0xFF)/255.0f; }
    if (blue) { *blue = ((baseValue >> 8) & 0xFF)/255.0f; }
    if (alpha) { *alpha = ((baseValue >> 0) & 0xFF)/255.0f; }
}

@end
