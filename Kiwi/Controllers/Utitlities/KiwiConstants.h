//
//  Constants.h
//  Kiwi
//
//  Created by Mohammed Salah on ٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#ifndef Kiwi_Constants_h
#define Kiwi_Constants_h


// Navigation Controller Buttons
#define NAVIGATION_BAR_SEARCH_BUTTON       @"NavSearch"
#define NAVIGATION_BAR_CART_BUTTON         @"NavCar"
#define NAVIGATION_BAR_SHARE_BUTTON        @"NavShare"
#define NAVIGATION_BAR_List_BUTTON         @"NavList"
#define NAVIGATION_BAR_RIGHT_CANCEL_BUTTON @"NavRCancel"
#define NAVIGATION_BAR_LEFT_CANCEL_BUTTON  @"NavLCancel"
#define NAVIGATION_BAR_LOCATION_BUTTON     @"NavLocation"
#define NAVIGATION_BAR_INFO_BUTTON         @"NavInfo"


//Colors
#define DEFUALT_COLOR [Utitlities ColorFromHexString:@"d61414"]
#define DARK_RED_COLOR  [Utitlities ColorFromHexString:@"aa0000"]
#define LIGHT_RED_COLOR  [Utitlities ColorFromHexString:@"cc0000"]
#define SEP_DARK_RED_COLOR  [Utitlities ColorFromHexString:@"930000"]
#define SEP_LIGHT_COLOR  [Utitlities ColorFromHexString:@"de5959"]
#endif


// images
#define DEFAULT_IMAGE @"no-image-500px.png"