//
//  Utitlities.h
//  Kiwi
//
//  Created by Mohammed Salah on ٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utitlities : NSObject


+(CGRect)getLableSizeFromText:(NSString*)text andLable:(UILabel*)lable;
+(UIColor *)ColorFromHexString:(NSString *) hexString;
+ (UIImage *)imageWithColor:(UIColor *)color;
+(double) getRundomNumber : (int) max andMin:(int)min isFloat:(BOOL)isfloat
;
+(NSURL *)imageUrl:(NSString*) imgUrl;
+(NSString*)getTag;
+(UIColor*) getRandomColor;
@end
