//
//  ProdcutsController.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Categories.h"
#import "KiwiProductDetails.h"
#import "KiwiWeeklyAds.h"
#import "LeftMenuController.h"

@protocol ProductsDelegate <NSObject>

-(void)RequestedProducts:(NSArray*)products isWeeklyAds:(BOOL)weeklyAds;
-(void)failToLoadProducts;
@end


@interface ProductsController : NSObject

@property (nonatomic,weak) id <ProductsDelegate> delegate;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;


-(int) numberOFProductsInCategoryAndChildern :(Categories*)category;
-(KiwiProductDetails*) getProductDetails:(NSString*) pro_id;
-(void)getProductsThumbnailForWeeklyAd:(KiwiWeeklyAds*) ad;
-(void)getProductsThumbnailForCateogry:(Categories*) category from:(int) start to:(int) numberOfItems withFilter:(NSDictionary*)currentFilter;
-(void)getProductsThumbnailForCateogryAndItsSubCats:(Categories*) category from:(int) start to:(int) numberOfItems withFilter:(NSDictionary*)currentFilter;
-(float)HighestPriceinCategory:(Categories*)category;



@end
