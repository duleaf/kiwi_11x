//
//  ProdcutsController.m
//  Kiwi
//
//  Created by Mohammed Salah on ٢٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "ProductsController.h"
#import "KiwiProductThumbnail.h"
#import "AppDelegate.h"
#import "Product.h"
#import "FilterController.h"

@interface ProductsController ()
{
    NSArray * products;
    FilterController * filter;
    LeftMenuController * leftmenuController;
}

@end

@implementation ProductsController



-(id)init
{
    self= [super init];
    
    if(self)
    {
        AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
        //2
        
        NSPersistentStoreCoordinator *coordinator = [appDelegate persistentStoreCoordinator];
        if (coordinator != nil) {
            self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            [self.managedObjectContext setPersistentStoreCoordinator: coordinator];
            leftmenuController = [[LeftMenuController alloc] init];
        }
        
    }
    
    return self;
    
}

-(NSMutableArray*)getAllSubCategoriesForCategory:(Categories*)category andFirstTime :(BOOL) firstTime;
{
    static NSMutableArray *childern ;
    
    if (firstTime)
    {
        childern = [[NSMutableArray alloc] init];
    }
    
    NSArray * subcats = [leftmenuController getMenuItemsWithParentId:category.cat_id];
    
    if(subcats.count < 1)
        [childern addObject:category];
    
    else
    for(Categories *subCat in subcats)
    {
        NSArray * secondLeve = [leftmenuController getMenuItemsWithParentId:subCat.cat_id];
        
        if(secondLeve.count < 1)
            [childern addObject:subCat];
        
        else
            [self getAllSubCategoriesForCategory:subCat andFirstTime:NO];
    }
    
    return childern;
}


-(int) numberOFProductsInCategoryAndChildern :(Categories*)category
{
    
    NSArray * cats = [self getAllSubCategoriesForCategory:category andFirstTime:YES];
    
    if(cats.count < 1)
        cats = [NSArray arrayWithObject:category];
    
    int totalNumberOfProducts = 0;
    
    for (Categories *cat in cats)
    {
        if(cat.cat_id.length > 0)
        totalNumberOfProducts += [self numberOFProductsInCategory:cat];
    }
    
    return  totalNumberOfProducts;
}


-(int) numberOFProductsInCategory :(Categories*)category
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
    NSPredicate * predicate=[NSPredicate predicateWithFormat:@"category==%@",category.cat_id]; // If required to fetch specific vehicle

    [fetchRequest setPredicate:predicate];
    [fetchRequest setEntity:entity];
    [fetchRequest setIncludesSubentities:NO]; //Omit subentities. Default is YES (i.e. include subentities)
    
    NSError *err;
    NSUInteger count = [self.managedObjectContext countForFetchRequest:fetchRequest error:&err];
    if(count == NSNotFound) {
        return  0;
    }
    
    
    return (int)count ;
}


-(float)HighestPriceinCategory:(Categories*)category
{
    float Price =0 ;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
    
    NSArray *allSubs = [self getAllSubCategoriesForCategory:category andFirstTime:YES];
    
    if(allSubs.count > 1)
    {
        
//        NSMutableArray *subpredicates = [NSMutableArray array];
//        
//        for (NSString *filterValue in allSubs)
//        {
//
//            NSPredicate *p = [NSPredicate predicateWithFormat:@"%K == %@", @"category", filterValue];
//            [subpredicates addObject:p];
//        }
//        NSPredicate *final = [NSCompoundPredicate andPredicateWithSubpredicates:subpredicates];
//    [fetchRequest setPredicate:final];
    }
    
    
    else
    {
        NSPredicate *final = [NSPredicate predicateWithFormat:@"%K == %@", @"category", category.cat_id];

        [fetchRequest setPredicate:final];
    }
    
    
    [fetchRequest setEntity:entity];
    
    NSExpression *keyPathExpression = [NSExpression expressionForKeyPath:@"price"];
    NSExpression *maxSalaryExpression = [NSExpression expressionForFunction:@"max:"
                                                                  arguments:[NSArray arrayWithObject:keyPathExpression]];

    
    NSExpressionDescription *expressionDescription = [[NSExpressionDescription alloc] init];
    [expressionDescription setName:@"price"];
    [expressionDescription setExpression:maxSalaryExpression];
    [expressionDescription setExpressionResultType:NSDecimalAttributeType];
    

    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:expressionDescription]];

        NSArray * array = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    Product * pro = [array lastObject] ;
    NSLog(@"PRICE : %@",pro.price);
    
    return [pro.price floatValue];
}

-(void)getProductsThumbnailForCateogryAndItsSubCats:(Categories*) category from:(int) start to:(int) numberOfItems withFilter:(NSDictionary*)currentFilter
{

    NSArray *allSubs = [self getAllSubCategoriesForCategory:category andFirstTime:YES];
    
    if(allSubs.count > 1)
    {
        int step = numberOfItems /4 ;
        
        for (int counter = 0 ; counter < numberOfItems; counter += step)
        {
            int randomCateogryIndex = [Utitlities getRundomNumber:((int)allSubs.count - 1) andMin:0 isFloat:NO];
            
            Categories * cat = [allSubs objectAtIndex:randomCateogryIndex];
            
            [self getProductsThumbnailForCateogry:cat from:0 to:step withFilter:currentFilter];
        }
    }
    
    else
    {
        [self getProductsThumbnailForCateogry:category from:start to:numberOfItems withFilter:currentFilter];
    }
    

    
    
}

-(void)getProductsThumbnailForCateogry:(Categories*) category from:(int) start to:(int) numberOfItems withFilter:(NSDictionary*)currentFilter
{
    
     NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
//    NSError *error;
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //1
//    NSString *documentsDirectory = [paths objectAtIndex:0]; //2
//    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"filter.plist"]; //3
//
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//
//    if (![fileManager fileExistsAtPath: path]) //4
//    {
//        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"filter" ofType:@"plist"]; //5
//
//        [fileManager copyItemAtPath:bundle toPath: path error:&error]; //6
//    }
//
//
//    NSMutableDictionary *currentFilter = [NSMutableDictionary dictionaryWithContentsOfFile:path];
//    
    NSPredicate *predicate;
    NSArray * filteredproducts;
    if([currentFilter allKeys].count > 0)
    {
        filter = [[FilterController alloc] init];
        filteredproducts = [filter getAllAvailableProductIdsForFilter:currentFilter];
    }
    
    
    
    if([[currentFilter allKeys] containsObject:@"price"])
    {
        NSString * Price = [currentFilter objectForKey:@"price"];
        NSString *lowerPrice = [[Price componentsSeparatedByString:@","] firstObject];
        NSString *upperPrice = [[Price componentsSeparatedByString:@","] objectAtIndex:1];
        
        predicate=[NSPredicate predicateWithFormat:@"category==%@ AND price <= %@ AND price>=%@",category.cat_id,upperPrice,lowerPrice]; // If required to fetch specific vehicle

    }
    
    else
        predicate=[NSPredicate predicateWithFormat:@"category==%@",category.cat_id]; // If required to fetch specific vehicle


    
    
    
//    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Product"];
    
    

    
    fetchRequest.predicate=predicate;
    
    NSLog(@"Count: %i",(int)[self.managedObjectContext countForFetchRequest:fetchRequest error:nil]);

    fetchRequest.fetchLimit = numberOfItems;
    fetchRequest.fetchOffset = start;
    
    
    //fsacctc2q73
    
    [self.managedObjectContext performBlockAndWait:^{

        products =[self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        
       
        
        NSMutableArray * kiwiProducts = [[NSMutableArray alloc] init];
        
       
        
        for (Product * pro in products)
        {
            if([currentFilter allKeys].count > 0 && filteredproducts.count > 0  && [filteredproducts containsObject:pro.name])
            {
                KiwiProductThumbnail * kiwiPro= [[KiwiProductThumbnail alloc] init];
                
                kiwiPro.name = pro.name;
                kiwiPro.price = pro.price;
                kiwiPro.product_id = pro.product_id;
                kiwiPro.attribute = [[[KiwiAttribute alloc] init] getProductFromId:pro.product_id];
                kiwiPro.tag = [[[KiwiProductTag alloc] init] getProductFromId:pro.product_id];
                
                kiwiPro.image = [[[KiwiProductImage alloc] init] getImageFromId:pro.product_id];
                
                [kiwiProducts addObject:kiwiPro];
            }
            else if ([currentFilter allKeys].count < 1)
            {
                KiwiProductThumbnail * kiwiPro= [[KiwiProductThumbnail alloc] init];
                
                kiwiPro.name = pro.name;
                kiwiPro.price = pro.price;
                kiwiPro.product_id = pro.product_id;
                kiwiPro.attribute = [[[KiwiAttribute alloc] init] getProductFromId:pro.product_id];
                kiwiPro.tag = [[[KiwiProductTag alloc] init] getProductFromId:pro.product_id];
            
                kiwiPro.image = [[[KiwiProductImage alloc] init] getImageFromId:pro.product_id];
                
                [kiwiProducts addObject:kiwiPro];
            }

        }
        
        [self.delegate RequestedProducts:[NSArray arrayWithArray:kiwiProducts] isWeeklyAds:NO];

    }];
    
}



-(void)getProductsThumbnailForWeeklyAd:(KiwiWeeklyAds*) ad
{

    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"weekly_id == %@",ad.weeklyAds_id];
    
    [fetchRequest setPredicate:predicate];

    [self.managedObjectContext performBlockAndWait:^{
   
        NSArray * products=[self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        
        
        
        NSMutableArray * kiwiProducts = [[NSMutableArray alloc] init];
        
        for (Product * pro in products)
        {
            KiwiProductThumbnail * kiwiPro= [[KiwiProductThumbnail alloc] init];
            
            kiwiPro.name = pro.name;
            kiwiPro.price = pro.price;
            kiwiPro.product_id = pro.product_id;
//            kiwiPro.attribute = [[[KiwiAttribute alloc] init] getProductFromId:pro.product_id];
            kiwiPro.tag = [[[KiwiProductTag alloc] init] getProductFromId:pro.product_id];
            kiwiPro.image = [[[KiwiProductImage alloc] init] getImageFromId:pro.product_id];
            kiwiPro.weekly_id = pro.weekly_id;
            [kiwiProducts addObject:kiwiPro];
        }
    
        [self.delegate RequestedProducts:[NSArray arrayWithArray:kiwiProducts] isWeeklyAds:YES];
    
    }];
    
  
    
}

-(KiwiProductDetails*) getProductDetails:(NSString*) pro_id
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Product" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"name == %@",pro_id];
    
    [fetchRequest setPredicate:predicate];
    Product * pro=[[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
    
    KiwiProductDetails * kiwiPro= [[KiwiProductDetails alloc] init];
    
    kiwiPro.name = pro.name;
    kiwiPro.price = pro.price;
    kiwiPro.product_id = pro.product_id;
    kiwiPro.attribute = [[[KiwiAttribute alloc] init] getProductFromId:pro.product_id];
    kiwiPro.tag = [[[KiwiProductTag alloc] init] getProductFromId:pro.product_id];
    kiwiPro.image = [[[KiwiProductImage alloc] init] getImageFromId:pro.product_id];
    
    
    return kiwiPro;
}

@end
