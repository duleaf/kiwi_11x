//
//  WeeklyAdsController.h
//  Kiwi
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WeeklyAds.h"
#import "KiwiWeeklyAds.h"
#import "AdImage.h"
@interface WeeklyAdsController : NSObject

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

-(NSArray*)getAllAds;
-(void)loadProducts;
-(void)removeProducts;
+(NSArray *) getWeeklyAds;
+(NSArray*) getWeeklyAdsProducts;
@end
