//
//  WeeklyAdsController.m
//  Kiwi
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "WeeklyAdsController.h"
#import "AppDelegate.h"
#import "ProductsController.h"
@interface WeeklyAdsController ()
{
    ProductsController * prodcutController;
}
@end

@implementation WeeklyAdsController

static  NSMutableArray * allProducts;
static NSArray *ads;

-(void)loadProducts
{
    allProducts = [[NSMutableArray alloc] init];
    
     ads = [self getAllAds];
    
    prodcutController = [[ProductsController alloc] init];
    
    for (KiwiWeeklyAds * ad in ads)
    {
        NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
        [myQueue addOperationWithBlock:^{
            prodcutController.delegate = self;
            [prodcutController getProductsThumbnailForWeeklyAd:ad];
        }];
    }
}

-(void)removeProducts
{
    allProducts = nil;
    ads = nil;
}


+(NSArray *) getWeeklyAds
{
    return ads;
}

+(NSArray*) getWeeklyAdsProducts
{
    return allProducts;
}

-(void)RequestedProducts:(NSArray*)products isWeeklyAds:(BOOL)weeklyAds
{
    [allProducts addObject:products];
    NSLog(@"Count: %i & Ads: %i",(int)allProducts.count , (int) ads.count);
    if(allProducts .count == ads.count)
        [KiwiNavigationBarController loadWeeklyAdsView];
  
}

-(id)init
{
    self= [super init];
    
    if(self)
    {
        AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
        //2
        NSPersistentStoreCoordinator *coordinator = [appDelegate persistentStoreCoordinator];
        if (coordinator != nil) {
            self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            [self.managedObjectContext setPersistentStoreCoordinator: coordinator];
        }    }
    
    return self;
    
}

-(NSArray*)getAllAds
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"WeeklyAds" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
 
   NSArray * ads= [self.managedObjectContext executeFetchRequest:fetchRequest error:nil] ;
    
    NSMutableArray * kiwiAds = [[NSMutableArray alloc] init];
    
    for (WeeklyAds * ad in ads)
    {
        KiwiWeeklyAds * kiwiad = [[KiwiWeeklyAds alloc] init];
        
        kiwiad.weeklyAds_id = ad.weeklyAd_id;
        kiwiad.startDate = ad.startDate;
        kiwiad.endDate = ad.endDate;
        
        KiwiAdImage *img = [[KiwiAdImage alloc] init];
        
        kiwiad.adImage = img;
        
        [kiwiAds addObject:kiwiad];
    }
    
    return [NSArray arrayWithArray:kiwiAds];
}

@end
