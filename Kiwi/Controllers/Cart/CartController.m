//
//  CartController.m
//  Kiwi
//
//  Created by Mohammed Salah on ١٤‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "CartController.h"
#import "CheckOutPageViewController.h"
@implementation CartController

static CartController *sharedInstance;


+(CartController*) getsharedInstance
{
    if(!sharedInstance)
    {
        sharedInstance = [[CartController alloc] init];
        sharedInstance.products = [[NSMutableArray alloc] init];
    }
    
    return sharedInstance;
}

-(void)addProduct:(KiwiProduct*)product
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"product_id contains[cd] %@", product.product_id];
    NSArray* filteredProducts = [self.products filteredArrayUsingPredicate:predicate];
    
    if (filteredProducts.count > 0)
    {
        KiwiProduct *pro = [filteredProducts firstObject];
        pro.productNumberOfPecies = [NSNumber numberWithInteger:pro.productNumberOfPecies.intValue + 1];
    }
    else
    {
        product.productNumberOfPecies = [NSNumber numberWithInt:1];
       [sharedInstance.products addObject:product];
    }
    

    [self updateCheckOutPage];
}

-(void)removeProduct:(int)product_index
{

    [[CartController getsharedInstance].products removeObjectAtIndex:product_index];
    CheckOutPageViewController * checkout = [KiwiNavigationBarController getcartView];
    [checkout calculateTotal];
    [KiwiNavigationBarController updateCartBadge:sharedInstance.products.count];

}


-(void)updateCheckOutPage
{
    CheckOutPageViewController * checkout = [KiwiNavigationBarController getcartView];
    [checkout.table reloadData];
    [checkout calculateTotal];
    
    if(sharedInstance.products.count > 0)
        
    [KiwiNavigationBarController updateCartBadge:sharedInstance.products.count];
}
@end
