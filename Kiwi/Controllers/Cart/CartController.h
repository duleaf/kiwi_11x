//
//  CartController.h
//  Kiwi
//
//  Created by Mohammed Salah on ١٤‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KiwiProduct.h"

@interface CartController : NSObject

@property (nonatomic,strong) NSMutableArray * products;

+(CartController*) getsharedInstance;
-(void)addProduct:(KiwiProduct*)product;
-(void)removeProduct:(int)product_index;
-(void)updateCheckOutPage;
@end
