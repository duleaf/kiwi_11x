//
//  LeftMenuController.h
//  Kiwi
//
//  Created by Mohammed Salah on ٢٥‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KiwiCategories.h"


@protocol AnimationDelegate <NSObject>

//-(void)animationOfType:(AnimationType)animType finishedWithParamters:(NSArray*)params;




@end


@interface LeftMenuController : NSObject

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, weak)   id <AnimationDelegate> delegate;
@property (nonatomic) int menuLevel;

-(NSArray*)getMenuItemsWithParentId:(NSString*)parentId;
-(void) itemPressedWithCategory:(KiwiCategories*)category ;

@end
