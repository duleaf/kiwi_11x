//
//  LeftMenuController.m
//  Kiwi
//
//  Created by Mohammed Salah on ٢٥‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "LeftMenuController.h"
#import "AppDelegate.h"
#import "KiwiCategories.h"
#import "Menu_Category.h"
#import "CenterViewController.h"

@interface LeftMenuController ()
{
    CenterViewController * centerViewController;
    
    
}
@end

@implementation LeftMenuController


- (id)init
{
    self = [super init];
    
    if (self) {
        //1
        AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
        //2
        self.managedObjectContext = appDelegate.managedObjectContext;
        
        centerViewController = [[CenterViewController alloc] init];
    }
    
    return self;
}

-(void) itemPressedWithCategory:(KiwiCategories*)category
{
    [centerViewController NewCategorySelected:category andLevel:self.menuLevel];
}

-(void)CellSelectedAtIndexPath: (NSIndexPath *)indexPath
{
    
    
}

-(NSArray*)getMenuItemsWithParentId:(NSString*)parentId
{
    NSFetchRequest *fetchRequest=[NSFetchRequest fetchRequestWithEntityName:@"Categories"];
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"parent_id==%@",parentId];
    fetchRequest.predicate=predicate;
    
    NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil] ;
    
    NSMutableArray * categories = [[NSMutableArray alloc] init];
    

    
    return items;
}

@end
