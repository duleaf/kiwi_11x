//
//  NavigationBarController.m
//  Kiwi
//
//  Created by Mohammed Salah on ٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "KiwiNavigationBarController.h"
#import "AppDelegate.h"
#import "UIBarButtonItem+Badge.h"
@implementation KiwiNavigationBarController

static UIView * NavBarView;
static UIButton * addToCart;
static UIBarButtonItem * cartViewButton;
static UIButton * search;
static UIButton * share;
static UIButton * addToList;
static UIButton * menu;
static KiwiNavigationBarController *sharedInstance;


+(KiwiNavigationBarController*)getSharedInstance
{
    if(!sharedInstance)
    {
        sharedInstance = [[KiwiNavigationBarController alloc] init];
        sharedInstance.storyBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
            }
    
    return sharedInstance;
}

+(void) loadWeeklyAdsView
{
    AppDelegate *app = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    
    LeftMenuView * leftMenu = (UIViewController*)app.viewController.leftPanel;
    
    [leftMenu weeklyAdsLoaded];
}

+(void)setAddToCart:(UIBarButtonItem*) cartButton andButton:(UIButton*)button
{
    addToCart = button;
    cartViewButton = cartButton;
    
    cartViewButton.target = sharedInstance;
    cartViewButton.action = @selector(NavCar);
    [addToCart addTarget:sharedInstance action:@selector(NavCar) forControlEvents:UIControlEventTouchUpInside];
}

+(void)updateCartBadge:(int) numberOfProducts
{
    if(numberOfProducts < 0)
        cartViewButton.badge.hidden = YES;
    else
    {
        cartViewButton.badge.hidden = NO;
        cartViewButton.badgeValue = [NSString stringWithFormat:@"%i",numberOfProducts];
    }
}

+(void)toggleLeftMenu
{
    AppDelegate *app = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    [app.viewController.centerPanel.sidePanelController toggleLeftPanel:nil];
}

+(UINavigationController*)getCenterViewNavigationController
{
    AppDelegate *app = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    UINavigationController *  naviation= (UINavigationController*) app.viewController.centerPanel;
    
    return naviation;
}

+(CheckOutPageViewController*)getcartView
{
    AppDelegate *app = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    UINavigationController *  naviation= (UINavigationController*) app.viewController.rightPanel;
    
    return (CheckOutPageViewController*)[naviation.viewControllers firstObject];
}

+(void)replaceCenterView:(UIViewController*)view
{
    UINavigationController * nav = [KiwiNavigationBarController getCenterViewNavigationController];
    
    nav.viewControllers = nil;
    
    nav.viewControllers = [NSArray arrayWithObject:view];
    view.navigationItem.rightBarButtonItem = cartViewButton;
}

+(void)initNavigaion
{
    UINavigationController * navigation = [KiwiNavigationBarController getCenterViewNavigationController];
    NavBarView = [[[NSBundle mainBundle] loadNibNamed:@"NavgationView" owner:nil options:nil] firstObject];
    NavBarView.frame = CGRectMake(154, 0, 166, 64);
    [navigation.view addSubview:NavBarView];
    
    menu = (UIButton*)[NavBarView viewWithTag:1];
    addToCart = (UIButton*)[NavBarView viewWithTag:2];
    search = (UIButton*)[NavBarView viewWithTag:3];
    share = (UIButton*)[NavBarView viewWithTag:4];
    share.hidden = YES;
    addToList = (UIButton*)[NavBarView viewWithTag:5];
    addToList.hidden = YES;
    [KiwiNavigationBarController getSharedInstance];
    
    [menu addTarget:sharedInstance action:@selector(NavMenu) forControlEvents:UIControlEventTouchUpInside];
    
}


//+(void)fitNavigationBarWithView:(UIViewController*) nextViewController
//{
//    UINavigationController * navigation = [KiwiNavigationBarController getCenterViewNavigationController];
//    
//    KiwiViewController * kiwiView = [navigation.viewControllers firstObject];
//    
//    NSMutableArray * listOfButtons = [kiwiView navigationBarButtons];
//    
//}


+(void)addButtonsToNvigationBar:(NSMutableArray*)buttons
{
    
//    for (int counter = 0; buttons.count > counter; counter++)
//    {
//        if()
//    }
}

#pragma NavigationBar Funcations

+(void)NavSearch
{
    
}

-(void)NavCar
{
    UIViewController * view = [[KiwiNavigationBarController getCenterViewNavigationController].viewControllers firstObject];

    [view presentViewController:[view.storyboard  instantiateViewControllerWithIdentifier:@"cartNav"] animated:YES completion:nil];
}



+(void)hidePresnetedView
{
    [[KiwiNavigationBarController getCenterViewNavigationController] dismissViewControllerAnimated:YES completion:nil];
}

+(void)NavShare
{
    
}

+(void)NavList
{
    
}

+(void)NavRCancel
{
    
}

+(void)NavLCancel
{
    
}

+(void)NavLocation
{
    
}

+(void)NavInfo
{
    
}

-(void)NavMenu
{
    AppDelegate *app = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
   [app.viewController.centerPanel.sidePanelController toggleLeftPanel:nil];
    
    
}


@end
