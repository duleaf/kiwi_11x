//
//  NavigationBarController.h
//  Kiwi
//
//  Created by Mohammed Salah on ٩‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LeftMenuView.h"
#import "CheckOutPageViewController.h"
#define NAV_BUTTON_WIDHT 30
#define NAV_BUTTON_HEIGHT 30


@interface KiwiNavigationBarController : NSObject

@property (nonatomic,strong) UIStoryboard * storyBoard;


+(void)initNavigaion;
+(UINavigationController*)getCenterViewNavigationController;
+(void)replaceCenterView:(UIViewController*)view;
+(void)hidePresnetedView;
+(KiwiNavigationBarController*)getSharedInstance;
-(void)NavCar;
+(void)setAddToCart:(UIBarButtonItem*) cartButton andButton:(UIButton*)button;
+(void)toggleLeftMenu;
+(CheckOutPageViewController*)getcartView;
+(void)updateCartBadge:(int) numberOfProducts;
+(void) loadWeeklyAdsView;

@end
