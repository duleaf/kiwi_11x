//
//  FilterController.h
//  Kiwi
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilterController : NSObject
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;


-(NSArray*) getAllAvailableAttributeswithCategory:(NSString*)cat_id;
-(NSMutableArray*) getAllAvailableVlauesForAttrubite:(NSString*)attribute withCategory:(NSString*)cat_id;
-(NSArray*) getAllAvailableProductIdsForFilter:(NSDictionary*)currentFilter;

@end
