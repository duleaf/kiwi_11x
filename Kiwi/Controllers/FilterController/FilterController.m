//
//  FilterController.m
//  Kiwi
//
//  Created by Mohammed Salah on ٨‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Duleaf. All rights reserved.
//

#import "FilterController.h"
#import "AppDelegate.h"

@implementation FilterController

-(id)init
{
    self = [super init];
    
    if (self)
    {
        AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
        //2
        self.managedObjectContext = appDelegate.managedObjectContext;
    }
    
    return  self;
}


-(NSMutableArray*) getAllAvailableVlauesForAttrubite:(NSString*)attribute withCategory:(NSString*)cat_id
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Attribute"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Attribute" inManagedObjectContext:self.managedObjectContext];
    
    // Required! Unless you set the resultType to NSDictionaryResultType, distinct can't work.
    // All objects in the backing store are implicitly distinct, but two dictionaries can be duplicates.
    // Since you only want distinct names, only ask for the 'name' property.
    
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"type== %@ AND category_id == %@",attribute , cat_id ];
    fetchRequest.predicate=predicate;
    
    
    fetchRequest.resultType = NSDictionaryResultType;
    fetchRequest.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"value"]];
    fetchRequest.returnsDistinctResults = YES;
    
    // Now it should yield an NSArray of distinct values in dictionaries.
    NSArray *dictionaries = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    NSLog (@"type: %@",dictionaries);
    
    NSMutableArray * allTypes = [[NSMutableArray alloc] init];
    
    for(NSDictionary * dic in dictionaries)
    {
        if([dic objectForKey:@"value"] != nil)
        [allTypes addObject:[dic objectForKey:@"value"]];
    }
    
    return allTypes;
}


-(NSArray*) getAllAvailableProductIdsForFilter:(NSDictionary*)currentFilter
{
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Attribute"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Attribute" inManagedObjectContext:self.managedObjectContext];
    
    // Required! Unless you set the resultType to NSDictionaryResultType, distinct can't work.
    // All objects in the backing store are implicitly distinct, but two dictionaries can be duplicates.
    // Since you only want distinct names, only ask for the 'name' property.
    NSArray * allKeys = [currentFilter allKeys];
    NSMutableArray *predicats = [[NSMutableArray alloc] init];
    
    for (NSString * key in allKeys)
    {
        if(![key isEqualToString:@"price"])
        {
            NSPredicate *p1 = [NSPredicate predicateWithFormat:@"type = %@ AND value = %@", key,[currentFilter objectForKey:key]];
            
            [predicats addObject:p1];
        }
        
    }
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates: predicats];

    
    fetchRequest.predicate=predicate;
    
    
    fetchRequest.resultType = NSDictionaryResultType;
    fetchRequest.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"product_id"]];
    fetchRequest.returnsDistinctResults = YES;
    
    // Now it should yield an NSArray of distinct values in dictionaries.
    NSArray *dictionaries = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    NSLog (@"type: %@",dictionaries);
    
    NSMutableArray * allTypes = [[NSMutableArray alloc] init];
    
    for(NSDictionary * dic in dictionaries)
    {
        if([dic objectForKey:@"product_id"] != nil)
            [allTypes addObject:[dic objectForKey:@"product_id"]];
    }
    
    return [NSArray arrayWithArray:allTypes];
}


-(NSArray*) getAllAvailableAttributeswithCategory:(NSString*)cat_id
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Attribute"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Attribute" inManagedObjectContext:self.managedObjectContext];
    
    // Required! Unless you set the resultType to NSDictionaryResultType, distinct can't work.
    // All objects in the backing store are implicitly distinct, but two dictionaries can be duplicates.
    // Since you only want distinct names, only ask for the 'name' property.
    
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"category_id == %@", cat_id ];
    
    [fetchRequest setPredicate:predicate];
    fetchRequest.resultType = NSDictionaryResultType;
    fetchRequest.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"type"]];
    fetchRequest.returnsDistinctResults = YES;
    
    // Now it should yield an NSArray of distinct values in dictionaries.
    NSArray *dictionaries = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    NSLog (@"type: %@",dictionaries);
    
    NSMutableArray * allTypes = [[NSMutableArray alloc] init];
    
    for(NSDictionary * dic in dictionaries)
    {
        if(!([[dic objectForKey:@"type"] isEqualToString:@"Color"] || [[dic objectForKey:@"type"] isEqualToString:@"Code"]))
        [allTypes addObject:[dic objectForKey:@"type"]];
    }
    
    return [NSArray arrayWithArray:allTypes];
}


@end
